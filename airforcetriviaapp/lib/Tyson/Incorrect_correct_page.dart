import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'Tysons_First_Page.dart';
import 'Tysons_globals.dart' as globals;

class IncorrectTextDesign extends StatelessWidget {
  const IncorrectTextDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Flexible(
          child: Text(
            'Incorrect',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 40,
              color: Colors.white,
            ),
          ),
        ),
        Flexible(
          child: Text(
            'X',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.red,
              fontSize: 50,
            ),
          ),
        ),
      ],
    );
  }
}

class IncorrectButton extends StatelessWidget {
  const IncorrectButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(244, 214, 218, 100),
            Color.fromRGBO(255, 0, 0, 100),
          ],
        ),
      ),
      child: const IncorrectTextDesign(),
    );
  }
}

class TysonsIncorrectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    globals.MarvelDCScore--;
    return Material(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color.fromARGB(255, 177, 44, 44),
          appBar: AppBar(
            centerTitle: true,
            toolbarHeight: MediaQuery.of(context).size.height * .1,
            title: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'Marvel/DC',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 60,
                  color: Color.fromARGB(255, 237, 237, 239),
                ),
              ),
            ),
            flexibleSpace: Container(
              color: const Color.fromRGBO(255, 217, 0, 0.965),
            ),
          ),
          body: GestureDetector(
            child: const IncorrectButton(),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const TysonStartPage()),
              );
            },
          ),
        ),
      ),
    );
  }
}

class CorrectTextDesign extends StatelessWidget {
  const CorrectTextDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AnimatedTextKit(
          animatedTexts: [
            ColorizeAnimatedText(
              'Correct',
              textStyle: TysonsColorizeTextStyle,
              colors: TysonsColorizeColors,
            ),
          ],
        ),
        const Icon(
          Icons.check_sharp,
          color: Color.fromRGBO(95, 210, 95, 100),
          size: 250,
        ),
      ],
    );
  }
}

class CorrectButton extends StatelessWidget {
  const CorrectButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(222, 240, 219, 100),
            Color.fromRGBO(36, 255, 0, 100),
          ],
        ),
      ),
      child: const CorrectTextDesign(),
    );
  }
}

class TysonsCorrectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color.fromARGB(255, 177, 44, 44),
          appBar: AppBar(
            centerTitle: true,
            toolbarHeight: MediaQuery.of(context).size.height * .1,
            title: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'Marvel/DC',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 60,
                  color: Color.fromARGB(255, 237, 237, 239),
                ),
              ),
            ),
            flexibleSpace: Container(
              color: const Color.fromRGBO(255, 217, 0, 0.965),
            ),
          ),
          body: GestureDetector(
            child: const CorrectButton(),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const TysonStartPage()),
              );
            },
          ),
        ),
      ),
    );
  }
}

List<MaterialColor> TysonsColorizeColors = [
  Colors.red,
  Colors.yellow,
  Colors.purple,
  Colors.blue,
];

const TysonsColorizeTextStyle = TextStyle(
  fontSize: 40.0,
  fontFamily: 'SF',
);
