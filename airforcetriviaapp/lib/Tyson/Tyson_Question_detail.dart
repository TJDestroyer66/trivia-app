import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import '../General/General_Question_Database.dart';
import 'Incorrect_correct_page.dart';
import 'dart:math';
import 'Tysons_globals.dart' as globals;

class QuestionDetail extends StatefulWidget {
  final Question question;

  const QuestionDetail({
    Key? key,
    required this.question,
  }) : super(key: key);

  @override
  State<QuestionDetail> createState() => _QuestionDetailState();
}

class _QuestionDetailState extends State<QuestionDetail> {
  @override
  Widget build(BuildContext context) {
    int count = 0;
    Random random = Random();
    int correctIndex = random.nextInt(4);
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color.fromARGB(255, 177, 44, 44),
        appBar: AppBar(
          centerTitle: true,
          toolbarHeight: MediaQuery.of(context).size.height * .1,
          title: const FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              'Marvel/DC',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 60,
                color: Color.fromARGB(255, 237, 237, 239),
              ),
            ),
          ),
          flexibleSpace: Container(
            color: const Color.fromRGBO(255, 217, 0, 0.965),
          ),
        ),
        body: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(15),
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                border: Border.all(
                  color: const Color.fromARGB(194, 0, 0, 0),
                  width: 3,
                ),
                gradient: const LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color.fromARGB(246, 255, 244, 244),
                    Color.fromARGB(248, 255, 255, 255),
                  ],
                ),
                borderRadius: const BorderRadius.all(Radius.circular(35)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(1),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: AnimatedTextKit(
                  totalRepeatCount: 1,
                  animatedTexts: [
                    TypewriterAnimatedText(widget.question.questionText,
                        textAlign: TextAlign.center,
                        textStyle: const TextStyle(
                          fontSize: 40,
                          color:  Color.fromARGB(194, 0, 0, 0),
                        ),
                        curve: Curves.easeIn,
                        speed: const Duration(milliseconds: 80)),
                  ],
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                padding: const EdgeInsets.only(top: 10),
                itemCount: widget.question.Incorrectanswers.length + 1,
                itemBuilder: (BuildContext context, int index) {
                  if (index == correctIndex) {
                    globals.MarvelDCScore++;
                    return optionButton(
                      context,
                      widget.question.Correctanswer,
                      () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TysonsCorrectPage()),
                        );
                      },
                    );
                  } else {
                    count++;
                    return optionButton(
                      context,
                      widget.question.Incorrectanswers[count - 1],
                      () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TysonsIncorrectPage()),
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget optionButton(
    BuildContext context, String option, Function pageNavigation) {
  return GestureDetector(
    child: Container(
      margin: const EdgeInsets.all(15),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromARGB(194, 0, 0, 0),
          width: 3,
        ),
        gradient: const LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromARGB(246, 255, 244, 244),
            Color.fromARGB(248, 255, 255, 255),
          ],
        ),
        borderRadius: const BorderRadius.all(Radius.circular(35)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: Align(
          alignment: Alignment.center,
          child: Text(
            option,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 32,
              color: Color.fromARGB(194, 0, 0, 0),
            ),
          ),
        ),
      ),
    ),
    onTap: () {
      pageNavigation();
    },
  );
}
