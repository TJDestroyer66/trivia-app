import 'package:airforcetriviaapp/main.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import '../Andrea/question A.dart';
import '../Jackson/DnD_Home_Page.dart';
import 'Tyson_Question_detail.dart';
import '../General/General_Question_Database.dart';
import 'Tysons_globals.dart' as globals;

//Tyson's question button
class TysonQuestionButtonTemplate extends StatelessWidget {
  TysonQuestionButtonTemplate({Key? key, required this.buttonIndex})
      : super(key: key);

  int buttonIndex;

  @override
  Widget build(BuildContext context) {
    const snackBar = SnackBar(content: Text('Question already answered!'));
    return GestureDetector(
      child: TysonQuestionButtonDesign(buttonIndex: buttonIndex),
      onTap: () {
        if (globals.MarvelDCansweredQuestions[buttonIndex] == false) {
          globals.MarvelDCansweredQuestions[buttonIndex] = true;
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return QuestionDetail(question: MarvelDCDatabase[buttonIndex]);
              },
            ),
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      },
    );
  }
}

//Tyson's question button design
class TysonQuestionButtonDesign extends StatelessWidget {
  TysonQuestionButtonDesign({Key? key, required this.buttonIndex})
      : super(key: key);

  int buttonIndex;
// Questions
  @override
  Widget build(BuildContext context) {
    int questionIndex = buttonIndex + 1;
    return Material(
      type: MaterialType.transparency,
      child: Container(
        margin: const EdgeInsets.all(15),
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          border: Border.all(
            color: const Color.fromARGB(194, 0, 0, 0),
            width: 3,
          ),
          gradient: const LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromARGB(246, 255, 244, 244),
              Color.fromARGB(248, 255, 255, 255),
            ],
          ),
          borderRadius: const BorderRadius.all(Radius.circular(35)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(1),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: AnimatedTextKit(
            animatedTexts: [
              WavyAnimatedText('Question $questionIndex',
                  textAlign: TextAlign.center,
                  textStyle: const TextStyle(
                    fontSize: 32,
                    color: Color.fromARGB(194, 0, 0, 0),
                  ),
                  speed: const Duration(milliseconds: 300)),
            ],
            totalRepeatCount: 1,
          ),
        ),
      ),
    );
  }
}

//This runs all the other classes before and formats them so they look nice.
class TysonStartPage extends StatefulWidget {
  const TysonStartPage({
    Key? key,
  }) : super(key: key);

  @override
  State<TysonStartPage> createState() => _TysonStartPageState();
}

class _TysonStartPageState extends State<TysonStartPage> {
  @override
  Widget build(BuildContext context) {
    int score = globals.MarvelDCScore;
    return Material(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color.fromARGB(255, 177, 44, 44),
          appBar: AppBar(
            centerTitle: true,
            toolbarHeight: MediaQuery.of(context).size.height * .1,
            title: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'Marvel/DC',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 60,
                  color: Color.fromARGB(255, 237, 237, 239),
                ),
              ),
            ),
            flexibleSpace: Container(
              color: const Color.fromRGBO(255, 217, 0, 0.965),
            ),
            leading: IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyApp()),
                );
              },
              icon: const Icon(
                Icons.house,
                size: 50,
              ),
            ),
          ),
          body: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: MarvelDCDatabase.length,
                  itemBuilder: (BuildContext context, int index) {
                    return TysonQuestionButtonTemplate(buttonIndex: index);
                  },
                ),
              ),
              Row(
                children: [
                  const Expanded(child: TysonPreviousButton()),
                  Expanded(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        'Score: $score/8',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  const Expanded(child: TysonNextButton()),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//Tyson's Next button
class TysonNextButton extends StatelessWidget {
  const TysonNextButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const TysonNextButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AWidget()),
        );
      },
    );
  }
}

//Tyson's Next button design
class TysonNextButtonDesign extends StatelessWidget {
  const TysonNextButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromRGBO(63, 63, 63, 62),
          width: 3,
        ),
        gradient: const LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(246, 255, 217, 0),
            Color.fromARGB(248, 239, 209, 73),
          ],
        ),
        borderRadius: const BorderRadius.all(Radius.circular(35)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: const Icon(
        Icons.keyboard_arrow_right_outlined,
        size: 55,
        color: Colors.white,
      ),
    );
  }
}

//Tyson's previous button
class TysonPreviousButton extends StatelessWidget {
  const TysonPreviousButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const TysonPreviousButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const DnDWidget()),
        );
      },
    );
  }
}

//Tyson's previous button design
class TysonPreviousButtonDesign extends StatelessWidget {
  const TysonPreviousButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromRGBO(63, 63, 63, 62),
          width: 3,
        ),
        gradient: const LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(246, 255, 217, 0),
            Color.fromARGB(248, 239, 209, 73),
          ],
        ),
        borderRadius: const BorderRadius.all(Radius.circular(35)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: const Icon(
        Icons.keyboard_arrow_left_outlined,
        size: 55,
        color: Colors.white,
      ),
    );
  }
}
