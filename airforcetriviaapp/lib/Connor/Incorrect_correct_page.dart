import 'package:flutter/material.dart';
import 'Connors_first_page.dart';
import 'Sports_globals.dart' as globals;

class ConnorIncorrectBackground extends StatelessWidget {
  const ConnorIncorrectBackground({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * .9,
        width: double.infinity,
        decoration: const BoxDecoration());
  }
}

class ConnorIncorrectButtonDesign extends StatelessWidget {
  const ConnorIncorrectButtonDesign({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Flexible(
          child: Text(
            '!!INCORRECT :(!!',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 40,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}

class ConnorIncorrectButton extends StatelessWidget {
  const ConnorIncorrectButton({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const ConnorIncorrectButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const CWidget()),
        );
      },
    );
  }
}

class ConnorsIncorrectPage extends StatelessWidget {
  const ConnorsIncorrectPage({super.key});

  @override
  Widget build(BuildContext context) {
    globals.SportsScore--;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(0, 0, 0, 100),
                Color.fromRGBO(255, 0, 0, 100),
              ],
            ),
          ),
          child: const ConnorIncorrectButton(),
        ),
      ),
    );
  }
}

class ConnorCorrectBackgorund extends StatelessWidget {
  const ConnorCorrectBackgorund({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * .9,
        width: double.infinity,
        decoration: const BoxDecoration());
  }
}

class ConnorCorrectButtonDesign extends StatelessWidget {
  const ConnorCorrectButtonDesign({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Flexible(
          child: Text(
            '!!CORRECT GOOD JOB #PROUDOFYOU!!',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 40,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}

class ConnorCorrectButton extends StatelessWidget {
  const ConnorCorrectButton({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const ConnorCorrectButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const CWidget()),
        );
      },
    );
  }
}

class ConnorsCorrectPage extends StatelessWidget {
  const ConnorsCorrectPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(0, 0, 0, 100),
                Color.fromRGBO(11, 255, 35, 100),
              ],
            ),
          ),
          child: const ConnorCorrectButton(),
        ),
      ),
    );
  }
}
