class Question {
  String imageUrl;
  //String Answer;

  List<String> Incorrectanswers;
  String Correctanswer;

  Question(this.imageUrl, this.Incorrectanswers, this.Correctanswer);

  static List<Question> questions = [
    Question(
      'When was baseball invented?',
      [
        '1976',
        '1982',
        '1902',
        '1850',
      ],
      '1876',
    ),
    Question(
      'When was football invented?',
      [
        '1905',
        '1910',
        '1855',
        '1890',
      ],
      '1920',
    ),
    Question(
      'What is the most popular sport?',
      [
        'Basketball',
        'Soccer',
        'Baseball',
        'Hockey',
      ],
      'Football',
    ),
    Question(
      'What is the longest baseball game ever played?',
      [
        '6 hours 30 min',
        '7 hours 15 min',
        '7 hours 35 min',
        '7 hours',
      ],
      '8 hours 6 min',
    ),
    Question(
      'What is the most liked sport in the world?',
      [
        'Baseball',
        'Basketball',
        'Hockey',
        'Football',
      ],
      'Soccer',
    ),
  ];
}
