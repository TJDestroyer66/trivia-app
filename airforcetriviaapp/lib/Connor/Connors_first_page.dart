import 'package:airforcetriviaapp/main.dart';
import 'package:flutter/material.dart';
import '../Airforce/Airforce_first_page.dart';
import '../Andrea/question A.dart';
import 'Connor_Question_detail.dart';
import 'Connor_Question_list.dart';
import 'Sports_globals.dart' as globals;

class CWidget extends StatelessWidget {
  const CWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: const Text("test"),
      home: const ConnorsStartPage(),
    );
  }
}

//This class make the background of the first page white
class ConnorBackground extends StatelessWidget {
  const ConnorBackground({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .900,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 226, 218, 0),
      ),
    );
  }
}

class ConnorsSportsTitle extends StatelessWidget {
  const ConnorsSportsTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width * 1,
        height: MediaQuery.of(context).size.height * .13,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/SPORTS/titleS.png'),
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}

//Connor's question button
class ConnorQuestionButtonTemplate extends StatelessWidget {
  ConnorQuestionButtonTemplate({Key? key, required this.buttonIndex})
      : super(key: key);

  int buttonIndex;

  @override
  Widget build(BuildContext context) {
    const snackbar = SnackBar(content: Text('Question already answered!'));
    return GestureDetector(
      child: ConnorQuestionButtonDesign(buttonIndex: buttonIndex),
      onTap: () {
        if (globals.answeredQuestions[buttonIndex] == false) {
          globals.answeredQuestions[buttonIndex] = true;
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return ConnorQuestionDetail(
                    question: Question.questions[buttonIndex]);
              },
            ),
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(snackbar);
        }
      },
    );
  }
}

//Connor's question button design
class ConnorQuestionButtonDesign extends StatelessWidget {
  ConnorQuestionButtonDesign({Key? key, required this.buttonIndex})
      : super(key: key);

  int buttonIndex;

  @override
  Widget build(BuildContext context) {
    int questionIndex = buttonIndex + 1;
    return Material(
      type: MaterialType.transparency,
      child: Container(
        margin: EdgeInsets.all(35),
        padding: EdgeInsets.all(35),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(15),
          ),
          color: Colors.black,
          border: Border.all(color: Colors.white, width: 5),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(1),
              spreadRadius: 3,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Text(
          'Question: $questionIndex',
          textAlign: TextAlign.center,
          style: const TextStyle(color: Colors.white, fontSize: 18),
        ),
      ),
    );
  }
}

//This class makes the user interface
class ConnorsStartPage extends StatefulWidget {
  const ConnorsStartPage({super.key});

  @override
  State<ConnorsStartPage> createState() => _ConnorsStartPageState();
}

class _ConnorsStartPageState extends State<ConnorsStartPage> {
  @override
  Widget build(BuildContext context) {
    int score = globals.SportsScore;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color.fromRGBO(255, 245, 0, 25),
        appBar: AppBar(
          centerTitle: true,
          title: const FittedBox(
            fit: BoxFit.scaleDown,
            child: Align(
              alignment: Alignment.center,
              child: Text(
                'Sports',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 45,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.black,
                  Color.fromRGBO(255, 245, 0, 25),
                ],
              ),
            ),
          ),
          leading: IconButton(
            onPressed: (() {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const HomePage()),
              );
            }),
            icon: const Icon(Icons.house),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: ListView.builder(
                itemCount: Question.questions.length,
                itemBuilder: (BuildContext context, int index) {
                  return ConnorQuestionButtonTemplate(buttonIndex: index);
                },
              ),
            ),
            Row(
              children: [
                const Expanded(child: BackButton()),
                Expanded(
                  child: Text(
                    'Score: $score/5',
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                const Expanded(child: NextButton()),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class NextButtonDesign extends StatelessWidget {
  const NextButtonDesign({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 5),
      padding: const EdgeInsets.only(top: 10, bottom: 5),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(
          Radius.circular(35),
        ),
        color: Colors.black,
        border: Border.all(color: Colors.white, width: 5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 3,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Icon(
          Icons.keyboard_arrow_right_outlined,
          size: 35,
          color: Colors.white,
        ),
      ),
    );
  }
}

class NextButton extends StatelessWidget {
  const NextButton({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const NextButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AFWidget()),
        );
      },
    );
  }
}

class BackButtonDesign extends StatelessWidget {
  const BackButtonDesign({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 5),
      padding: const EdgeInsets.only(top: 10, bottom: 5),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(
          Radius.circular(35),
        ),
        color: Colors.black,
        border: Border.all(color: Colors.white, width: 5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 3,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Icon(
          Icons.keyboard_arrow_left_outlined,
          size: 35,
          color: Colors.white,
        ),
      ),
    );
  }
}

class BackButton extends StatelessWidget {
  const BackButton({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const BackButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AWidget()),
        );
      },
    );
  }
}
