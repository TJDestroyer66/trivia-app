import 'dart:math';

import 'package:airforcetriviaapp/Connor/Incorrect_correct_page.dart';
import 'package:flutter/material.dart';
import 'Connor_Question_list.dart';
import 'Sports_globals.dart' as globals;

class ConnorQuestionDetail extends StatefulWidget {
  final Question question;

  const ConnorQuestionDetail({super.key, required this.question});

  @override
  State<ConnorQuestionDetail> createState() => _ConnorQuestionDetailState();
}

class _ConnorQuestionDetailState extends State<ConnorQuestionDetail> {
  @override
  Widget build(BuildContext context) {
    int count = 0;
    Random random = Random();
    int correctIndex = random.nextInt(5);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.yellow,
        appBar: AppBar(
          centerTitle: true,
          title: const FittedBox(
            fit: BoxFit.scaleDown,
            child: Align(
              alignment: Alignment.center,
              child: Text(
                'Sports',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 45,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.black,
                  Color.fromRGBO(255, 245, 0, 25),
                ],
              ),
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(15),
              padding: EdgeInsets.all(10),
              height: MediaQuery.of(context).size.height * 0.1,
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 4),
                color: Colors.black,
                borderRadius: const BorderRadius.all(
                  Radius.circular(25),
                ),
              ),
              child: Text(
                widget.question.imageUrl,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 40,
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: widget.question.Incorrectanswers.length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == correctIndex) {
                      globals.SportsScore++;
                      return optionButton(
                          context, widget.question.Correctanswer, () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ConnorsCorrectPage()),
                        );
                      });
                    } else {
                      count++;
                      //globals.SportsScore--;
                      return optionButton(
                          context, widget.question.Incorrectanswers[count - 1],
                          () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  const ConnorsIncorrectPage()),
                        );
                      });
                    }
                  }),
            )
          ],
        ),
      ),
    );
  }
}

Widget optionButton(
    BuildContext context, String option, Function pageNavigation) {
  return GestureDetector(
    child: Container(
      margin: EdgeInsets.all(15),
      padding: EdgeInsets.all(15),
      height: MediaQuery.of(context).size.height * .1,
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.white, width: 4),
        color: Colors.black,
        borderRadius: const BorderRadius.all(
          Radius.circular(25),
        ),
      ),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          option,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 32,
            color: Color.fromARGB(255, 255, 255, 255),
          ),
        ),
      ),
    ),
    onTap: () {
      pageNavigation();
    },
  );
}
