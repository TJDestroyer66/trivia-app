import 'package:airforcetriviaapp/Airforce/Airforce_first_page.dart';
import 'package:airforcetriviaapp/Connor/Incorrect_correct_page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Correct_Page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Incorrect_Page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'DnD_Home_Page.dart';
import 'DnD_Question_Buttons.dart';
import 'DnDQuestion1.dart';


class QuestionFormat8 extends StatelessWidget {
  const QuestionFormat8({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/D&D/DnD_Background.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0.6),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [DnDBanner(), QuestionWidget8(), QuestionScrollable8()],
          ),
        ),
      ),
    );
  }
}

class QuestionWidget8 extends StatelessWidget {
  const QuestionWidget8({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (225),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Center(
                  child: Text(
                    "Which Ability Score is the Perception skill associated with?",
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,)
                  ))),
        ));
  }
}

class QuestionScrollable8 extends StatelessWidget {
  const QuestionScrollable8({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          Question8Answer1(),
          Question8Answer2(),
          Question8Answer3(),
          Question8Answer4(),
          InQuestionBackButton(),
        ],
      ),
    );
  }
}

class Question8Answer1 extends StatelessWidget {
  const Question8Answer1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question8Answer1Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDIncorrectBackground()),
        );
      },
    );
  }
}

class Question8Answer1Design extends StatelessWidget {
  const Question8Answer1Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Intelligence",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question8Answer2 extends StatelessWidget {
  const Question8Answer2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question8Answer2Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDIncorrectBackground()),
        );
      },
    );
  }
}

class Question8Answer2Design extends StatelessWidget {
  const Question8Answer2Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Charisma",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question8Answer3 extends StatelessWidget {
  const Question8Answer3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question8Answer3Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDIncorrectBackground()),
        );
      },
    );
  }
}

class Question8Answer3Design extends StatelessWidget {
  const Question8Answer3Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Dexterity",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question8Answer4 extends StatelessWidget {
  const Question8Answer4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question8Answer4Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDCorrectBackground()),
        );
      },
    );
  }
}

class Question8Answer4Design extends StatelessWidget {
  const Question8Answer4Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Wisdom",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}