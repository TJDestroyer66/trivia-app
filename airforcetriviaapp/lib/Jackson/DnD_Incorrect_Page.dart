import 'package:airforcetriviaapp/Airforce/Airforce_first_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'DnD_Home_Page.dart';
import 'DnD_Question_Buttons.dart';


class DnDIncorrectBackground extends StatelessWidget {
  const DnDIncorrectBackground({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/D&D/DnD_Incorrect_Background.PNG'),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [DnDBanner(),
            Spacer(),
            Container(child: const Padding(
              padding: EdgeInsets.only(bottom:80),
              child: NextButtonIncorrect())) ],
          ),
        ),
      ),
    );
  }
}


class NextButtonIncorrect extends StatelessWidget {
  const NextButtonIncorrect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const DnDNextButtonDesignIncorrect(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDWidget()),
        );
      },
    );
  }
}

class DnDNextButtonDesignIncorrect extends StatelessWidget {
  const DnDNextButtonDesignIncorrect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 155, 1, 1)),
                child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Try Again?",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  )
                )
        ),
      )
    );
  }
}
