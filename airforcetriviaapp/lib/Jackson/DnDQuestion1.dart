import 'package:airforcetriviaapp/Airforce/Airforce_first_page.dart';
import 'package:airforcetriviaapp/Connor/Incorrect_correct_page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Correct_Page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Incorrect_Page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'DnD_Home_Page.dart';
import 'DnD_Question_Buttons.dart';


class QuestionFormat1 extends StatelessWidget {
  const QuestionFormat1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/D&D/DnD_Background.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0.6),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [DnDBanner(), QuestionWidget(), QuestionScrollable()],
          ),
        ),
      ),
    );
  }
}

class QuestionWidget extends StatelessWidget {
  const QuestionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (225),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Center(
                  child: Text(
                    "How many Official Classes are there in DnD 5th Edition?",
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,)
                  ))),
        ));
  }
}

class QuestionScrollable extends StatelessWidget {
  const QuestionScrollable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          Question1Answer1(),
          Question1Answer2(),
          Question1Answer3(),
          Question1Answer4(),
          InQuestionBackButton(),
        ],
      ),
    );
  }
}

class Question1Answer1 extends StatelessWidget {
  const Question1Answer1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question1Answer1Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDIncorrectBackground()),
        );
      },
    );
  }
}

class Question1Answer1Design extends StatelessWidget {
  const Question1Answer1Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "12",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question1Answer2 extends StatelessWidget {
  const Question1Answer2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question1Answer2Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDCorrectBackground()),
        );
      },
    );
  }
}

class Question1Answer2Design extends StatelessWidget {
  const Question1Answer2Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "13",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question1Answer3 extends StatelessWidget {
  const Question1Answer3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question1Answer3Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDIncorrectBackground()),
        );
      },
    );
  }
}

class Question1Answer3Design extends StatelessWidget {
  const Question1Answer3Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "14",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question1Answer4 extends StatelessWidget {
  const Question1Answer4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question1Answer4Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDIncorrectBackground()),
        );
      },
    );
  }
}

class Question1Answer4Design extends StatelessWidget {
  const Question1Answer4Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "15",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class InQuestionBackButton extends StatelessWidget {
  const InQuestionBackButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const InQuestionBackButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDWidget()),
        );
      },
    );
  }
}

class InQuestionBackButtonDesign extends StatelessWidget {
  const InQuestionBackButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "<< Back",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}
