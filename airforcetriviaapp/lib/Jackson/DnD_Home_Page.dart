import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'DnD_Question_Buttons.dart';
import 'DnD_Home_Page.dart';

class DnDWidget extends StatelessWidget {
  const DnDWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: const DnDPage(),
    );
  }
}

class DnDPage extends StatelessWidget {
  const DnDPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/D&D/DnD_Background.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0.6),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [DnDBanner(), Scrollable(), HomeBar()],
          ),
        ),
      ),
    );
  }
}

class DnDBanner extends StatelessWidget {
  const DnDBanner({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * .1,
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromARGB(255, 255, 0, 0),
              Color.fromARGB(255, 250, 229, 229),
            ]),
      ),
      child: Row(children: [
        Expanded(
          flex: 1,
          child: const Image(
            image: AssetImage('assets/D&D/D20_Outline.png'),
          ),
        ),
        Expanded(
          flex: 10,
          child: FittedBox(
            fit: BoxFit.fitHeight,
            child: Padding(
                padding: EdgeInsets.fromLTRB(3, 2, 3, 2),
                child: Text("Dungeons & Dragons",
                    style: GoogleFonts.dancingScript(color: Colors.white))),
          ),
        ),
      ]),
    );
  }
}

class Scrollable extends StatelessWidget {
  const Scrollable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          Question1(),
          Question2(),
          Question3(),
          Question4(),
          Question5(),
          Question6(),
          Question7(),
          Question8(),
          Question9(),
          Question10(),
        ],
      ),
    );
  }
}

class HomeBar extends StatelessWidget {
  const HomeBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (400),
      height: (100),
      child: Row(children: [
        Expanded(
          flex: 1,
          child: FittedBox(
            fit: BoxFit.fitHeight,
            child: Padding(
                padding: EdgeInsets.fromLTRB(3, 2, 3, 2),
                child: DnDBackButton()),
          ),
        ),
        Expanded(
          flex: 2,
          child: FittedBox(
            fit: BoxFit.fitHeight,
            child: Padding(
                padding: EdgeInsets.fromLTRB(3, 2, 3, 2),
                child: DnDHomeButton()),
          ),
        ),
        Expanded(
          flex: 1,
          child:FittedBox(
            fit: BoxFit.fitHeight,
            child: Padding(
                padding: EdgeInsets.fromLTRB(3, 2, 3, 2),
                child: DnDNextButton()),
          ),
        ),
      ]),
    );
  }
}