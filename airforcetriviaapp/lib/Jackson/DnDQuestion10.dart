import 'package:airforcetriviaapp/Airforce/Airforce_first_page.dart';
import 'package:airforcetriviaapp/Connor/Incorrect_correct_page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Correct_Page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Incorrect_Page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'DnD_Home_Page.dart';
import 'DnD_Question_Buttons.dart';
import 'DnDQuestion1.dart';


class QuestionFormat10 extends StatelessWidget {
  const QuestionFormat10({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/D&D/DnD_Background.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0.6),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [DnDBanner(), QuestionWidget10(), QuestionScrollable10()],
          ),
        ),
      ),
    );
  }
}

class QuestionWidget10 extends StatelessWidget {
  const QuestionWidget10({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (225),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Center(
                  child: Text(
                    "How many different types of dice are needed to play DnD?",
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,)
                  ))),
        ));
  }
}

class QuestionScrollable10 extends StatelessWidget {
  const QuestionScrollable10({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          Question10Answer1(),
          Question10Answer2(),
          Question10Answer3(),
          Question10Answer4(),
          InQuestionBackButton(),
        ],
      ),
    );
  }
}

class Question10Answer1 extends StatelessWidget {
  const Question10Answer1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question10Answer1Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDIncorrectBackground()),
        );
      },
    );
  }
}

class Question10Answer1Design extends StatelessWidget {
  const Question10Answer1Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "5",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question10Answer2 extends StatelessWidget {
  const Question10Answer2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question10Answer2Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDCorrectBackground()),
        );
      },
    );
  }
}

class Question10Answer2Design extends StatelessWidget {
  const Question10Answer2Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "7",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question10Answer3 extends StatelessWidget {
  const Question10Answer3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question10Answer3Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDIncorrectBackground()),
        );
      },
    );
  }
}

class Question10Answer3Design extends StatelessWidget {
  const Question10Answer3Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "8",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question10Answer4 extends StatelessWidget {
  const Question10Answer4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question10Answer4Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DnDIncorrectBackground()),
        );
      },
    );
  }
}

class Question10Answer4Design extends StatelessWidget {
  const Question10Answer4Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "11",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dancingScript(color: Colors.white),
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}