class Question {
  String imageUrl;

  List<String> Incorrectanswers;
  String Correctanswer;

  Question(this.imageUrl, this.Incorrectanswers, this.Correctanswer);

  static List<Question> questions = 
  [
    
    Question(
      
      'How many types of planes does the air force use?',
      [
        '63',
        '47',
        '38',
      ],
        '39',
    ),
    Question(
      "how many people work on Hill Air Force Base?",
      [
        '20,000',
        '30,000',
        '18,000',
      ],
        '22,000',
    ),
    Question(
      'Which planes does Hill Air Force Base preform maintence on?',
      [
        'F-35',
        'F-16',
        'A-10',
      ],
        'All of the above',
    ),
    Question(
      'What year was Hill Air Force Base created?',
      [
        '1935',
        '1936',
        '1938',
      ],
        '1939',
    ),
    Question(
      'Which is higher than a group',
      [
        'Squadron',
        'Section',
        'All of the above',
      ],
        'Wing',
    ),
    Question(
      "Which of these are higher ranked Air Force Oficers?",
      [
        'Captian',
        'Colonel',
        'Major',
      ],
        'Lieutenant General',
    ),
    Question(
      "when was the Air Force founded",
      [
        "September 8, 1947",
        'September 18, 1946',
        'September 8 1947',
      ],
        'September 18, 1947',
    ),
  ];
  
  
}
