import 'package:flutter/material.dart';
import 'dart:math';
import 'CorrectPage.dart';
import 'Airforce_questions.dart';
import 'Airforce_globals.dart' as globals;

class CustomTitle extends StatelessWidget {
  const CustomTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .1,
      width: double.infinity,
      decoration: const BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/AIRFORCE/titleA.png'),
        fit: BoxFit.fill,
      )),
    );
  }
}

class QuestionDetail extends StatefulWidget {
  final Question question;

  const QuestionDetail({Key? key, required this.question}) : super(key: key);

  @override
  State<QuestionDetail> createState() => _QuestionDetailState();
}

class _QuestionDetailState extends State<QuestionDetail> {
  @override
  Widget build(BuildContext context) {
    int count = 0;
    Random random = Random();
    int correctIndex = random.nextInt(4);
    // 1
    return Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      const CustomTitle(),
      Container(
        padding: EdgeInsets.all(90),
        margin: EdgeInsets.all(80),
        height: MediaQuery.of(context).size.height * 0.3,
        width:  MediaQuery.of(context).size.height * 0.6,
        
        decoration: BoxDecoration(
          border: Border.all(
            color: const Color.fromRGBO(63, 63, 63, 62),
            width: 3,
          ),
          gradient: const LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromARGB(255, 165, 210, 243),
              Color.fromRGBO(255, 255, 255, 7),
            ],
          ),
          borderRadius: const BorderRadius.all(Radius.circular(35)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(1),
              spreadRadius: 5,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Text(widget.question.imageUrl,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 45,
              color: Color.fromARGB(255, 9, 0, 65),
              fontWeight: FontWeight.bold
            )),
      ),
      Expanded(
        child: ListView.builder(
            itemCount: widget.question.Incorrectanswers.length + 1,
            itemBuilder: (BuildContext context, int index) {
              if (index == correctIndex) {
                globals.AirForceScore++;
                return optionButton(context, widget.question.Correctanswer, () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const InAirForceCorrectButton()),
                  );
                });
              } else {
                count++;
                //globals.SuperherosScore--;
                return optionButton(
                    context, widget.question.Incorrectanswers[count - 1], () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const InAirForceinCorrectButton()),
                  );
                });
              }
            }),
      )
    ])));
  }
}

Widget optionButton(
    BuildContext context, String option, Function pageNavigation) {
  return GestureDetector(
    child: Container(
      padding: EdgeInsets.all(4),
      margin: EdgeInsets.all(5.9),
      height: MediaQuery.of(context).size.height * 0.1,
        width:  MediaQuery.of(context).size.height * .05,
      decoration: BoxDecoration(
        color: Color.fromARGB(217, 41, 15, 206),
        borderRadius: BorderRadius.circular(60),
        border: Border.all(
            color: Color.fromARGB(255, 45, 18, 221), width: 6),
      ),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          option,
          textAlign: TextAlign.center,
          style: const TextStyle(
              fontSize: 50,
              color: Color.fromARGB(255, 204, 204, 217),
              fontWeight: FontWeight.bold),
        ),
      ),
    ),
    onTap: () {
      pageNavigation();
    },
  );
}
