import '../Tyson/Incorrect_correct_page.dart';
import 'Airforce_first_page.dart';
import 'Airforce_globals.dart' as globals;
import 'package:flutter/material.dart';

class InAirForceCorrectButtonDesign extends StatelessWidget {
  const InAirForceCorrectButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(222, 240, 219, 100),
            Color.fromRGBO(36, 255, 0, 100),
          ],
        ),
      ),
      child: const CorrectTextDesign(),
    );
  }
}
 @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Flexible(
          child: Text(
            'Incorrect',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 40,
              color: Colors.white,
            ),
          ),
        ),
        Flexible(
          child: Text(
            'X',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.red,
              fontSize: 50,
            ),
          ),
        ),
      ],
    );
  }


class IncorrectButton extends StatelessWidget {
  const IncorrectButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromRGBO(244, 214, 218, 100),
              Color.fromRGBO(255, 0, 0, 100),
            ],
          ),
        ),
        child: const IncorrectTextDesign(),
      ),
    );
  }
}

class TysonsIncorrectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    globals.AirForceScore--;
    return Material(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color.fromARGB(255, 177, 44, 44),
          appBar: AppBar(
            centerTitle: true,
            toolbarHeight: MediaQuery.of(context).size.height * .1,
            title: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'Marvel/DC',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 60,
                  color: Color.fromARGB(255, 237, 237, 239),
                ),
              ),
            ),
            flexibleSpace: Container(
              color: const Color.fromRGBO(255, 217, 0, 0.965),
            ),
          ),
          body: GestureDetector(
            child: const IncorrectButton(),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AFPage()),
              );
            },
          ),
        ),
      ),
    );
  }
}


class InAirForceCorrectButton extends StatelessWidget {
  const InAirForceCorrectButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const InAirForceCorrectButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AFPage()),
        );
      },
    );
  }
}
class InAirForceinCorrectButton extends StatelessWidget {
  const InAirForceinCorrectButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const IncorrectButton(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AFPage()),
        );
      },
    );
  }
}

class CustomIncorrectBackground extends StatelessWidget {
  const CustomIncorrectBackground({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * .859,
        width: double.infinity,
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage(
              'assets/SUPERHEROS/Incorrect_correct/TysonsIncorrectpage.png'),
          fit: BoxFit.fill,
        )));
  }
}

class AirForceIncorrectPage extends StatefulWidget {
  const AirForceIncorrectPage({Key? key}) : super(key: key);

  @override
  State<AirForceIncorrectPage> createState() => _AirForceIncorrectPageState();
}

class _AirForceIncorrectPageState extends State<AirForceIncorrectPage> {

  @override
  Widget build(BuildContext context) {
    int count = 0;
    globals.AirForceScore--;
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const AirForceCustomTitle(),
              Stack(
                children: const [
                  CustomIncorrectBackground(),
                  InAirForceCorrectButton(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AirForceCorrectButtonDesign extends StatelessWidget {
  const AirForceCorrectButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .8,
      width: double.infinity,
      child: const Image(
        image:
            AssetImage('assets/SUPERHEROS/Incorrect_correct/CorrectText.png'),
      ),
    );
  }
}

class AirForceCorrectButton extends StatelessWidget {
  const AirForceCorrectButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const AirForceCorrectButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AFPage()),
        );
      },
    );
  }
}

class AirForceAirForceAirForceCustomCorrectBackground extends StatelessWidget {
  const AirForceAirForceAirForceCustomCorrectBackground({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * .859,
        width: double.infinity,
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage(
              'assets/SUPERHEROS/Incorrect_correct/TysonsCorrectpage.png'),
          fit: BoxFit.fill,
        )));
  }
}

class AirforceCorrectPage extends StatelessWidget {
  const AirforceCorrectPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const AirForceCustomTitle(),
              Stack(
                children: const [
                  AirForceAirForceAirForceCustomCorrectBackground(),
                  AirForceCorrectButton(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
