import 'package:airforcetriviaapp/Airforce/Airforce_questions.dart';
import 'package:airforcetriviaapp/main.dart';
import 'package:flutter/material.dart';
import '../Connor/Connors_first_page.dart';
import '../Jackson/DnD_Home_Page.dart';
import 'Airforce_globals.dart' as globals;
import 'Airforcequestion_details.dart';
import 'package:airforcetriviaapp/StarWars/SW_Home_Page.dart';

class AFWidget extends StatelessWidget {
  const AFWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: AFPage(),
    );
  }
}

class AirForceCustomTitle extends StatelessWidget {

  const AirForceCustomTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .1,
      width: double.infinity,
      decoration: const BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/AIRFORCE/title-AF.png'),
        fit: BoxFit.fill,
      )),
    );
  }
}

class AirforceQuestionbuttondesign extends StatelessWidget {
   AirforceQuestionbuttondesign({Key? key, required this.buttonIndex})
      : super(key: key);

  int buttonIndex;
   @override
  Widget build(BuildContext context) {
    int quesitonIndex = buttonIndex + 1;
    return Material(
      type: MaterialType.transparency,
      child: Container(
        margin: const EdgeInsets.only(top: 30, bottom: 15),
        padding: const EdgeInsets.only(top: 30, bottom: 15),
        decoration: BoxDecoration(
          border: Border.all(
            color: const Color.fromRGBO(63, 63, 63, 62),
            width: 3,
          ),
          gradient: const LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromRGBO(36, 0, 255, 10),
              Color.fromRGBO(36, 0, 255, 10),
            ],
          ),
          borderRadius: const BorderRadius.all(Radius.circular(35)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(1),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Align(
          alignment: Alignment.center,
          child: Text(
            'Question $quesitonIndex',
            style: const TextStyle(
              fontSize: 35,
              color: Color.fromARGB(255, 204, 204, 217),
            ),
          ),
        ),
      ),
    );
  }
}
 
  class AirForceQuestionTemplete extends StatelessWidget {
  AirForceQuestionTemplete({super.key, required this.buttonIndex});
int buttonIndex;
  @override
  Widget build(BuildContext context) {
    const snackbar = SnackBar(content: Text('Question already answered!'));
    return GestureDetector(
      child: AirforceQuestionbuttondesign(buttonIndex: buttonIndex),
      onTap: () {
        if (globals.answeredQuestions[buttonIndex] == false) {
          globals.answeredQuestions[buttonIndex] = true;
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return QuestionDetail(
                    question: Question.questions[buttonIndex]);
              },
            ),
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(snackbar);
        }
      },
    );
  }
}
  
class AirForcePreviousButton extends StatelessWidget {
  const AirForcePreviousButton({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const AirForcePreviousButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const ConnorsStartPage()),
        );
      },
    );
  }
}
class AirForcePreviousButtonDesign extends StatelessWidget {
  const AirForcePreviousButtonDesign({super.key});

  @override
  Widget build(BuildContext context) {
   return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromRGBO(63, 63, 63, 62),
          width: 3,
        ),
        gradient: const LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(36, 0, 255, 10),
            Color.fromRGBO(36, 0, 255, 10),
          ],
        ),
        borderRadius: const BorderRadius.all(Radius.circular(35)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: const Icon(
        Icons.keyboard_arrow_left_outlined,
        size: 55,
        color: Color.fromARGB(255, 204, 204, 217),
      ),
    );
  }
}
class AirForceNextButton extends StatelessWidget {
  const AirForceNextButton({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const  AirForceNextButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const SWWidget()),
        );
      },
    );
  }
}
class AirForceNextButtonDesign extends StatelessWidget {
  const AirForceNextButtonDesign({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromRGBO(63, 63, 63, 62),
          width: 3,
        ),
        gradient: const LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(36, 0, 255, 10),
            Color.fromRGBO(36, 0, 255, 10),
          ],
        ),
        borderRadius: const BorderRadius.all(Radius.circular(35)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: const Icon(
        Icons.keyboard_arrow_right_outlined,
        size: 55,
        color: Color.fromARGB(255, 204, 204, 217),
      ),
    );
  }
}


class AFPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int score = globals.AirForceScore;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text(
              'Air Force',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 30,
                color: Colors.white,
              ),
            ),
            flexibleSpace: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color.fromRGBO(36, 0, 255, 10),
                    Color.fromRGBO(36, 0, 255, 10),
                  ],
                ),
              ),
            ),
            leading: IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const MyApp()),
                  );
                },
                icon: const Icon(Icons.house)),
          ),
        body: Column(
          children: [
            Expanded(
              //height: MediaQuery.of(context).size.height * .9,
              child: ListView.builder(
                itemCount: Question.questions.length,
                itemBuilder: (BuildContext context, int index) {
                  return AirForceQuestionTemplete(buttonIndex: index);
                },
              ),
            ),
            Row(
              children: [
                const Expanded(child: AirForcePreviousButton()),
                Expanded(
                  child: Text(
                    'Score: $score/5',
                    style: const TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                const Expanded(child:  AirForceNextButton()),
              ],
            ),
            //],
            //),
            //],
            //),
          ],
        ),
      ),
    );
  }
}
