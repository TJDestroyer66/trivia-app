import 'package:airforcetriviaapp/Andrea/firstquestions.dart';
import 'package:airforcetriviaapp/Andrea/question%20A.dart';
import 'package:airforcetriviaapp/Andrea/six.dart';
import 'package:airforcetriviaapp/Andrea/wrong.dart';
import 'package:airforcetriviaapp/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'CorrectPage.dart';
import 'Question4.dart';

class Fiwidget  extends StatelessWidget {
  const Fiwidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const AndreasPage(),
    );
  }
}




// the title CANDY
class TitleCandy extends StatelessWidget {
  const TitleCandy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: const Image(
        image: AssetImage('assets/CANDY/title1.png'),
      ),
    );
  }
}

// the Question box outline/ layout
class QA5 extends StatelessWidget {
  const QA5({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(0),
       margin: const EdgeInsets.fromLTRB(10,20,10,1),

      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .25,
      
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          'What is the most hated candy in the world?',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}


// option one for the question
class Option1 extends StatelessWidget {
  const Option1({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30,10,20,5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          'Candy Corn',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
// this is the button to Candy Corn which will go to correct
class CandyCorn extends StatelessWidget {
  const CandyCorn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Option1(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const COwidget()),
        );
      },
    );
  }
}
// option two for the question
class Option2 extends StatelessWidget {
  const Option2({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30,10,20,5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          'Black licorice ',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
// this is the button to Black Licorice  which will go to wrong
class BlackLicorice  extends StatelessWidget {
  const BlackLicorice ({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Option2(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const WRwidget()),
        );
      },
    );
  }
}
// option three for the question
class Option3 extends StatelessWidget {
  const Option3({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30,10,20,5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          'Smarties',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
// this is the button to Smarties which will go to correct
class  Smarties  extends StatelessWidget {
  const Smarties({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Option3(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const WRwidget()),
        );
      },
    );
  }
}

// option three for the question
class Option4 extends StatelessWidget {
  const Option4({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30,10,20,5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          'Tootsie Rolls',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
// this is the button to Tootsie Rolls which will go to wrong
class TootsieRolls extends StatelessWidget {
  const TootsieRolls({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Option4(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const WRwidget()),
        );
      },
    );
  }
}



// background color
class BG extends StatelessWidget {
  const BG({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        // constraints: BoxConstraints.expand(),
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(240, 163, 163, 1),
                Color.fromARGB(192, 192, 201, 167)
              ]),
        ),
      ),
    );
  }
}

// Foward button
class Foward extends StatelessWidget {
  const Foward({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(35),
      margin: const EdgeInsets.fromLTRB(270, 570, 0, 0),
      child: const Image(
        image: AssetImage('assets/CANDY/bear.png'),
      ),
    );
  }
}


// class for the Foward
class FowardButton extends StatelessWidget {
  const FowardButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Foward(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Sxwidget()),
        );
      },
    );
  }
}
// this is going to be the next button
class NextB extends StatelessWidget {
  const NextB({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom:5),
      padding: const EdgeInsets.only(top: 10, bottom: 5),
      width: MediaQuery.of(context).size.width * .2,
      height: MediaQuery.of(context).size.height * .07,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '-->',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
// next button function
class NEXTf extends StatelessWidget {
  const NEXTf({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const NextB(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Sxwidget()),
        );
      },
    );
  }
}


// this is printing/ stacking the pictures
class AndreasPage extends StatelessWidget {
  const AndreasPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: [
          const BG(),
          
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const [
              TitleCandy(),
              QA5(),
              CandyCorn(),
              BlackLicorice(),
              Smarties(),

              TootsieRolls(),

              // OP3(),
              // OP4(),
            ],
          ),
          Row(children:const [
       
          Expanded(
                  child: Align(alignment: Alignment.bottomLeft, child: Back())),
              Expanded(
                  child:
                      Align(alignment: Alignment.bottomCenter, child: HOMEB())),
              Expanded(
                  child:
                      Align(alignment: Alignment.bottomRight, child: NEXTf())),
             
      
      
      ]
      
      )
        ]),
      ),
    );
  }
}
// this is going to be the home button
class Hbutton extends StatelessWidget {
  const Hbutton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
       margin: const EdgeInsets.only(top: 10, bottom:5),
      padding: const EdgeInsets.only(top: 10, bottom: 5),
      width: MediaQuery.of(context).size.width * .2,
      height: MediaQuery.of(context).size.height * .07,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Icon(
        Icons.house_outlined,
        size: 90,
        color: Colors.white,
      ),
      ),
    );
  }
}

// home button function
class HOMEB extends StatelessWidget {
  const HOMEB({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Hbutton(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const MyApp()),
        );
      },
    );
  }
}
// 
class BACK extends StatelessWidget {
  const BACK({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 5),
      padding: const EdgeInsets.only(top: 10, bottom: 5),
      width: MediaQuery.of(context).size.width * .2,
      height: MediaQuery.of(context).size.height * .07,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '<--',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

// back function
class Back extends StatelessWidget {
  const Back({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const BACK(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Frwidget()),
        );
      },
    );
  }
}