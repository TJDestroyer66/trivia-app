import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Swidget  extends StatelessWidget {
  const Swidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const AndreasPage(),
    );
  }
}

// option one for the question
class OP1 extends StatelessWidget {
  const OP1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: const Image(
        image: AssetImage('assets/CANDY/option.png'),
      ),
    );
  }
}

// option two for the question
class OP2 extends StatelessWidget {
  const OP2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: const Image(
        image: AssetImage('assets/CANDY/option.png'),
      ),
    );
  }
}

// option three for the question
class OP3 extends StatelessWidget {
  const OP3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: const Image(
        image: AssetImage('assets/CANDY/option.png'),
      ),
    );
  }
}

// option four for the question
class OP4 extends StatelessWidget {
  const OP4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: const Image(
        image: AssetImage('assets/CANDY/option.png'),
      ),
    );
  }
}

// the title CANDY
class TitleCandy extends StatelessWidget {
  const TitleCandy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: const Image(
        image: AssetImage('assets/CANDY/title1.png'),
      ),
    );
  }
}

// the Question box outline/ layout
class QA extends StatelessWidget {
  const QA({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {;
    return Container(
      padding: const EdgeInsets.all(40),
       margin: const EdgeInsets.fromLTRB(10,20,10,1),

      width: MediaQuery.of(context).size.width * 1,
      
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
        child: const Text(
          "What country invented chocolate?",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontSize: 35),
        ),
      );
  }
}

class CandyTextBox extends StatelessWidget {
  CandyTextBox(this.myText);
  String myText;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(30),
      margin: const EdgeInsets.fromLTRB(10,10,10,5),
      width: MediaQuery.of(context).size.width * 1,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: Text(
        myText,
        textAlign: TextAlign.center,
        style: const TextStyle(color: Colors.white, fontSize: 30,
        fontWeight: FontWeight.bold,),
        
       
      ),
    );
  }
}

// background color
class BG extends StatelessWidget {
  const BG({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        // constraints: BoxConstraints.expand(),
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(240, 163, 163, 1),
                Color.fromARGB(192, 192, 201, 167)
              ]),
        ),
      ),
    );
  }
}

// this is printing/ stacking the pictures
class AndreasPage extends StatelessWidget {
  const AndreasPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Stack(children: [
            const BG(),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [ 
                const TitleCandy(),
                const QA(),
                // OP1(),
                // OP2(),
                // OP3(),
                // OP4(),
                CandyTextBox(" Norway"),
                CandyTextBox("Switzerland"),
                CandyTextBox("Mexico"),
                CandyTextBox("Belgium"),
              ],
            ),
          ]),
        ),
      ),
    );
  }
}
