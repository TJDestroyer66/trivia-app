import 'package:airforcetriviaapp/Andrea/firstquestions.dart';
import 'package:airforcetriviaapp/Andrea/second%20.dart';
import 'package:airforcetriviaapp/Andrea/six.dart';
import 'package:airforcetriviaapp/Andrea/thirdquestion.dart';
import 'package:airforcetriviaapp/main.dart';
import 'package:flutter/material.dart';

import '../Connor/Connors_first_page.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import '../Tyson/Tysons_First_Page.dart';
import 'Question4.dart';
import 'five.dart';
//

class AWidget extends StatelessWidget {
  const AWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const AndreasPage(),
    );
  }
}

// Next button
class Next extends StatelessWidget {
  const Next({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(310, 20, 0, 0),
      child: const Image(
        image: AssetImage('assets/CANDY/next.png'),
      ),
    );
  }
}

// class for the next
class NextButton extends StatelessWidget {
  const NextButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Next(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const CWidget()),
        );
      },
    );
  }
}

// class for the home
class HomeButton extends StatelessWidget {
  const HomeButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Home(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const MyApp()),
        );
      },
    );
  }
}

// Home button
class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.fromLTRB(1, 50, 280, 1),
        child: const FittedBox(
          fit: BoxFit.scaleDown,
          child: Image(
            image: AssetImage('assets/CANDY/home.png'),
          ),
        ));
  }
}

// Question 2
class CandyTextBox extends StatelessWidget {
  const CandyTextBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30, 10, 20, 5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '1',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

// this is the button "tap"1
class Qone extends StatelessWidget {
  const Qone({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const CandyTextBox(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Fwidget()),
        );
      },
    );
  }
}

// Question 2
class CandyTextBox2 extends StatelessWidget {
  const CandyTextBox2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30, 10, 20, 5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '2',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

// this is the button "tap"2
class Qsecond extends StatelessWidget {
  const Qsecond({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const CandyTextBox2(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Secwidget()),
        );
      },
    );
  }
}

// Questions 3
class CandyTextBox3 extends StatelessWidget {
  const CandyTextBox3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30, 10, 20, 5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '3',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

// this is the button "tap"3
class Qthird extends StatelessWidget {
  const Qthird({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const CandyTextBox3(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Thwidget()),
        );
      },
    );
  }
}

//Q4
class CandyTextBox4 extends StatelessWidget {
  const CandyTextBox4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30, 10, 20, 5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '4',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

// this is the button "tap"4
class QFourth extends StatelessWidget {
  const QFourth({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const CandyTextBox4(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Frwidget()),
        );
      },
    );
  }
}

// this is the button "tap"5
class QF extends StatelessWidget {
  const QF({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const CandyTextBox5(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Fiwidget()),
        );
      },
    );
  }
}

// Q5
class CandyTextBox5 extends StatelessWidget {
  const CandyTextBox5({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30, 10, 20, 5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '5',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

//Q6
class CandyTextBox6 extends StatelessWidget {
  const CandyTextBox6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(30, 10, 20, 5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '6',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

// this is going to be the home button
class Hbutton extends StatelessWidget {
  const Hbutton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 5),
      padding: const EdgeInsets.only(top: 10, bottom: 5),
      width: MediaQuery.of(context).size.width * .2,
      height: MediaQuery.of(context).size.height * .07,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Icon(
          Icons.house_outlined,
          size: 90,
          color: Colors.white,
        ),
      ),
    );
  }
}

// home button function
class HOMEB extends StatelessWidget {
  const HOMEB({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Hbutton(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const MyApp()),
        );
      },
    );
  }
}

// this is the button "tap"6
class QSI extends StatelessWidget {
  const QSI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const CandyTextBox6(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Sxwidget()),
        );
      },
    );
  }
}

// Q5
class CANDY extends StatelessWidget {
  const CANDY({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300.0,
      child: TextLiquidFill(
        text: 'CANDY',
        waveColor: Colors.brown,
        boxBackgroundColor: const Color(0xffDB9F9F),
        textStyle: const TextStyle(
          fontSize: 80.0,
          fontWeight: FontWeight.bold,
        ),
        boxHeight: 100.0,
      ),
    );
  }
}

// Next button

// this is going to be the next button
class NextB extends StatelessWidget {
  const NextB({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 5),
      padding: const EdgeInsets.only(top: 10, bottom: 5),
      width: MediaQuery.of(context).size.width * .2,
      height: MediaQuery.of(context).size.height * .07,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '-->',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

// home next function
class NEXTf extends StatelessWidget {
  const NEXTf({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const NextB(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const CWidget()),
        );
      },
    );
  }
}

// this is going to be the Tysons  button
class BACK extends StatelessWidget {
  const BACK({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 5),
      padding: const EdgeInsets.only(top: 10, bottom: 5),
      width: MediaQuery.of(context).size.width * .2,
      height: MediaQuery.of(context).size.height * .07,
      decoration: BoxDecoration(
        color: const Color(0xffDB9F9F),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: const Color(0x77704D4D), width: 3),
      ),
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          '<--',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

// back function
class Back extends StatelessWidget {
  const Back({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const BACK(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const TysonStartPage()),
        );
      },
    );
  }
}

// bottom candy
class BCANDY extends StatelessWidget {
  const BCANDY({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: const EdgeInsets.all(.5),
      margin: const EdgeInsets.fromLTRB(15, 595, 5, 5),
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      child: const FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          'CANDY',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Color.fromRGBO(197, 108, 108, 40),
            fontSize: 100,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

class PINK extends StatelessWidget {
  const PINK({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(900),
      height: MediaQuery.of(context).size.height * 85.9,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Color(0xffDB9F9F),
      ),
    );
  }
}

// background
class BG extends StatelessWidget {
  const BG({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(350),
      margin: const EdgeInsets.fromLTRB(30, 10, 20, 5),
      width: MediaQuery.of(context).size.width * 1,
      decoration: BoxDecoration(
        gradient: const LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromRGBO(207, 231, 216, 1),
              Color.fromRGBO(154, 150, 205, 25)
            ]),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
            color: const Color.fromRGBO(222, 162, 169, 47), width: 13),
      ),
    );
  }
}

//
class BGC extends StatelessWidget {
  const BGC({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(350),
      margin: const EdgeInsets.fromLTRB(30, 10, 20, 5),
      width: MediaQuery.of(context).size.width * 1,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
            color: const Color.fromRGBO(222, 162, 169, 47), width: 13),
      ),
    );
  }
}

// this is printing/ stacking the pictures
class AndreasPage extends StatelessWidget {
  const AndreasPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Stack(children: [
            const PINK(),
            const BG(),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                CANDY(),
                Qone(),
                Qsecond(),
                Qthird(),
                QFourth(),
                QF(),
                QSI(),
              ],
            ),
            Row(children: const [
              Expanded(
                  child: Align(alignment: Alignment.bottomLeft, child: Back())),
              Expanded(
                  child:
                      Align(alignment: Alignment.bottomCenter, child: HOMEB())),
              Expanded(
                  child:
                      Align(alignment: Alignment.bottomRight, child: NEXTf())),
            ])
          ]),
        ),
      ),
    );
  }
}
