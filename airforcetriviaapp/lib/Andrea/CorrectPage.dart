
import 'package:airforcetriviaapp/Andrea/question%20A.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
// this is the correct widget 
class COwidget extends StatelessWidget {
  const COwidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const AndreasPage(),
    );
  }
}










// correct
class CorrectB extends StatelessWidget {
  CorrectB({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {const colorizeColors = [
  Colors.green,
  Colors.lightGreen,
  Color.fromARGB(255, 129, 121, 54),
  Colors.yellow,
];

const colorizeTextStyle = TextStyle(
  fontSize: 85.0,
  fontFamily: 'Horizon',
  fontWeight: FontWeight.bold
);
    return SizedBox(
  width: 350.0,
  child: AnimatedTextKit(
    animatedTexts: [
      ColorizeAnimatedText(
        '!GOOD JOB!',
        textStyle: colorizeTextStyle,
        colors: colorizeColors,
      ),
      ColorizeAnimatedText(
        'YOU GOT IT',
        textStyle: colorizeTextStyle,
        colors: colorizeColors,
      ),
      ColorizeAnimatedText(
        'Click Me',
        textStyle: colorizeTextStyle,
        colors: colorizeColors,
      ),
    ],
    isRepeatingAnimation: true, 
    onTap: () { Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AWidget()),
        );
    },
  ),
);
  }
}
// This is to the homepage
class WOW extends StatelessWidget {
  const WOW({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: CorrectB(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AWidget()),
        );
      },
    );
  }
}






// background color
class BG extends StatelessWidget {
  const BG({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        // constraints: BoxConstraints.expand(),
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(55, 71, 51, 1),
                Color.fromRGBO(141, 217, 122, 0)
              ]),
        ),
      ),
    );
  }
}

// this is printing/ stacking the pictures
class AndreasPage extends StatelessWidget {
  const AndreasPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Stack(children: [
            const BG(),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [ 
                

               
               Expanded(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child:WOW()
                  )),

              ],
            ),
          ]),
        ),
      ),
    );
  }
}
