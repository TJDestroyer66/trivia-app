import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'SW_Home_Page.dart';
import 'SW_Question_Buttons.dart';
import 'SWIncorrect.dart';
import 'SWCorrect.dart';

class SWQuestionFormat1 extends StatelessWidget {
  const SWQuestionFormat1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(231, 117, 42, 237),
            Color.fromARGB(231, 232, 62, 93),
          ],
        ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0.6),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [SWBanner(), QuestionWidget(), QuestionScrollable()],
          ),
        ),
      ),
    );
  }
}

class QuestionWidget extends StatelessWidget {
  const QuestionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (225),
              width: (300),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Center(
                      child: Text(
                    "C-3P0 is fluent in how many languages?",
                    textScaleFactor: 1.5,
                  )))),
        ));
  }
}

class QuestionScrollable extends StatelessWidget {
  const QuestionScrollable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          Question1Answer1(),
          Question1Answer2(),
          Question1Answer3(),
          Question1Answer4(),
          InQuestionBackButton(),
        ],
      ),
    );
  }
}

class Question1Answer1 extends StatelessWidget {
  const Question1Answer1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question1Answer1Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question1Answer1Design extends StatelessWidget {
  const Question1Answer1Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Over 600 million",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question1Answer2 extends StatelessWidget {
  const Question1Answer2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question1Answer2Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWCorrectBackground()),
        );
      },
    );
  }
}

class Question1Answer2Design extends StatelessWidget {
  const Question1Answer2Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Over 60 million",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question1Answer3 extends StatelessWidget {
  const Question1Answer3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question1Answer3Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question1Answer3Design extends StatelessWidget {
  const Question1Answer3Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Over 50 million",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question1Answer4 extends StatelessWidget {
  const Question1Answer4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question1Answer4Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question1Answer4Design extends StatelessWidget {
  const Question1Answer4Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Over 500 million",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class InQuestionBackButton extends StatelessWidget {
  const InQuestionBackButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const InQuestionBackButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWWidget()),
        );
      },
    );
  }
}

class InQuestionBackButtonDesign extends StatelessWidget {
  const InQuestionBackButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "<< Back",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}
