import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'SW_Home_Page.dart';
import 'SW_Question_Buttons.dart';
import 'SWQuestion1.dart';
import 'SWIncorrect.dart';
import 'SWCorrect.dart';


class SWQuestionFormat2 extends StatelessWidget {
  const SWQuestionFormat2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
         gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(231, 117, 42, 237),
            Color.fromARGB(231, 232, 62, 93),
          ],
        ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0.6),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [SWBanner(), QuestionWidget2(), QuestionScrollable2()],
          ),
        ),
      ),
    );
  }
}

class QuestionWidget2 extends StatelessWidget {
  const QuestionWidget2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (225),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Center(
                  child: Text(
                    "What planet does Yoda live on?",
                    textScaleFactor: 1.5,)
                  ))),
        ));
  }
}

class QuestionScrollable2 extends StatelessWidget {
  const QuestionScrollable2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          Question2Answer1(),
          Question2Answer2(),
          Question2Answer3(),
          Question2Answer4(),
          InQuestionBackButton(),
        ],
      ),
    );
  }
}

class Question2Answer1 extends StatelessWidget {
  const Question2Answer1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question2Answer1Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question2Answer1Design extends StatelessWidget {
  const Question2Answer1Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Coruscant",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}

class Question2Answer2 extends StatelessWidget {
  const Question2Answer2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question2Answer2Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWCorrectBackground()),
        );
      },
    );
  }
}

class Question2Answer2Design extends StatelessWidget {
  const Question2Answer2Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Dagobah",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}

class Question2Answer3 extends StatelessWidget {
  const Question2Answer3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question2Answer3Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question2Answer3Design extends StatelessWidget {
  const Question2Answer3Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Kashyyyk",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}

class Question2Answer4 extends StatelessWidget {
  const Question2Answer4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question2Answer4Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question2Answer4Design extends StatelessWidget {
  const Question2Answer4Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Geonosis",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}