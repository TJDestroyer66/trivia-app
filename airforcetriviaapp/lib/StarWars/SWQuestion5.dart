import 'package:airforcetriviaapp/Airforce/Airforce_first_page.dart';
import 'package:airforcetriviaapp/Connor/Incorrect_correct_page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Correct_Page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Incorrect_Page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'SWQuestion1.dart';
import 'SWIncorrect.dart';
import 'SWCorrect.dart';
import 'SW_Home_Page.dart';


class SWQuestionFormat5 extends StatelessWidget {
  const SWQuestionFormat5({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(231, 117, 42, 237),
            Color.fromARGB(231, 232, 62, 93),
          ],
        ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0.6),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [SWBanner(), QuestionWidget5(), QuestionScrollable5()],
          ),
        ),
      ),
    );
  }
}

class QuestionWidget5 extends StatelessWidget {
  const QuestionWidget5({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (225),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Center(
                  child: Text(
                    "What is the name of Boba Fett's Ship?",
                    textScaleFactor: 1.5,)
                  ))),
        ));
  }
}

class QuestionScrollable5 extends StatelessWidget {
  const QuestionScrollable5({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          Question5Answer1(),
          Question5Answer2(),
          Question5Answer3(),
          Question5Answer4(),
          InQuestionBackButton(),
        ],
      ),
    );
  }
}

class Question5Answer1 extends StatelessWidget {
  const Question5Answer1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question5Answer1Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question5Answer1Design extends StatelessWidget {
  const Question5Answer1Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Moonlighter",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}

class Question5Answer2 extends StatelessWidget {
  const Question5Answer2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question5Answer2Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWCorrectBackground()),
        );
      },
    );
  }
}

class Question5Answer2Design extends StatelessWidget {
  const Question5Answer2Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Slave 1",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question5Answer3 extends StatelessWidget {
  const Question5Answer3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question5Answer3Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question5Answer3Design extends StatelessWidget {
  const Question5Answer3Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Scarlet Dragon",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question5Answer4 extends StatelessWidget {
  const Question5Answer4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question5Answer4Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question5Answer4Design extends StatelessWidget {
  const Question5Answer4Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Argo III",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}