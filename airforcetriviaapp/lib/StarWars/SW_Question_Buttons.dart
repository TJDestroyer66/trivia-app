import 'package:airforcetriviaapp/Jackson/DnD_Home_Page.dart';
import 'package:airforcetriviaapp/StarWars/SWCorrect.dart';
import 'package:airforcetriviaapp/Tyson/Tysons_First_Page.dart';
import 'package:airforcetriviaapp/main.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:airforcetriviaapp/StarWars/SWIncorrect.dart';
import 'package:airforcetriviaapp/StarWars/SW_Home_Page.dart';
import 'SWQuestion1.dart';
import 'SWQuestion2.dart';
import 'SWQuestion3.dart';
import 'SWQuestion4.dart';
import 'SWQuestion5.dart';
import 'package:airforcetriviaapp/Airforce/Airforce_first_page.dart';

class SWQuestion1 extends StatelessWidget {
  const SWQuestion1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question1Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWQuestionFormat1()),
        );
      },
    );
  }
}

class Question1Design extends StatelessWidget {
  const Question1Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (100),
              width: (400),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Question 1",
                    textAlign: TextAlign.center,
                    textScaleFactor: 2.5,
                  ))),
        ));
  }
}

class SWQuestion2 extends StatelessWidget {
  const SWQuestion2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question2Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const SWQuestionFormat2()),
        );
      },
    );
  }
}

class Question2Design extends StatelessWidget {
  const Question2Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (100),
              width: (400),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Question 2",
                    textAlign: TextAlign.center,
                    textScaleFactor: 2.5,
                  ))),
        ));
  }
}

class SWQuestion3 extends StatelessWidget {
  const SWQuestion3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question3Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const SWQuestionFormat3()),
        );
      },
    );
  }
}

class Question3Design extends StatelessWidget {
  const Question3Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
               height: (100),
              width: (400),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Question 3",
                    textAlign: TextAlign.center,
                    textScaleFactor: 2.5,
                  ))),
        ));
  }
}

class SWQuestion4 extends StatelessWidget {
  const SWQuestion4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question4Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const SWQuestionFormat4()),
        );
      },
    );
  }
}

class Question4Design extends StatelessWidget {
  const Question4Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (100),
              width: (400),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Question 4",
                    textAlign: TextAlign.center,
                    textScaleFactor: 2.5,
                  ))),
        ));
  }
}

class SWQuestion5 extends StatelessWidget {
  const SWQuestion5({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question5Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const SWQuestionFormat5()),
        );
      },
    );
  }
}

class Question5Design extends StatelessWidget {
  const Question5Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (100),
              width: (400),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Question 5",
                    textAlign: TextAlign.center,
                    textScaleFactor: 2.5,
                  ))),
        ));
  }
}

class SWBackButton extends StatelessWidget {
  const SWBackButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const SWBackButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AFWidget()),
        );
      },
    );
  }
}

class SWBackButtonDesign extends StatelessWidget {
  const SWBackButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (100),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Back",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class SWHomeButton extends StatelessWidget {
  const SWHomeButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const SWHomeButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const MyApp()),
        );
      },
    );
  }
}

class SWHomeButtonDesign extends StatelessWidget {
  const SWHomeButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (200),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Home",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class SWNextButton extends StatelessWidget {
  const SWNextButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const SWNextButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const DnDWidget()),
        );
      },
    );
  }
}

class SWNextButtonDesign extends StatelessWidget {
  const SWNextButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (100),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Next",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}
