import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'SW_Question_Buttons.dart';
import 'SW_Home_Page.dart';

class SWWidget extends StatelessWidget {
  const SWWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: const SWPage(),
    );
  }
}

class SWPage extends StatelessWidget {
  const SWPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(231, 117, 42, 237),
            Color.fromARGB(231, 232, 62, 93),
          ],
        ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0.6),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [SWBanner(), SWScrollable(), SWHomeBar()],
          ),
        ),
      ),
    );
  }
}

class SWBanner extends StatelessWidget {
  const SWBanner({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * .1,
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
         gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(231, 117, 42, 237),
            Color.fromARGB(231, 232, 62, 93),
          ],
        ),
      ),
      child: Center(
      child: Text(
        'Star Wars',
        textAlign: TextAlign.center,
          style: TextStyle(
                  fontSize: 60,
                  color: Color.fromARGB(255, 237, 237, 239),
                ),
      ),
      ),
    );
  }
}

class SWScrollable extends StatelessWidget {
  const SWScrollable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          SWQuestion1(),
          SWQuestion2(),
          SWQuestion3(),
          SWQuestion4(),
          SWQuestion5(),
        ],
      ),
    );
  }
}

class SWHomeBar extends StatelessWidget {
  const SWHomeBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (400),
      height: (100),
      child: Row(children: [
        Expanded(
          flex: 1,
          child: FittedBox(
            fit: BoxFit.fitHeight,
            child: Padding(
                padding: EdgeInsets.fromLTRB(3, 2, 3, 2),
                child: SWBackButton()),
          ),
        ),
        Expanded(
          flex: 2,
          child: FittedBox(
            fit: BoxFit.fitHeight,
            child: Padding(
                padding: EdgeInsets.fromLTRB(3, 2, 3, 2),
                child: SWHomeButton()),
          ),
        ),
        Expanded(
          flex: 1,
          child:FittedBox(
            fit: BoxFit.fitHeight,
            child: Padding(
                padding: EdgeInsets.fromLTRB(3, 2, 3, 2),
                child: SWNextButton()),
          ),
        ),
      ]),
    );
  }
}