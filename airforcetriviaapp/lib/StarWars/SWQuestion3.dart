import 'package:airforcetriviaapp/Airforce/Airforce_first_page.dart';
import 'package:airforcetriviaapp/Connor/Incorrect_correct_page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Correct_Page.dart';
import 'package:airforcetriviaapp/Jackson/DnD_Incorrect_Page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'SWQuestion1.dart';
import 'SWIncorrect.dart';
import 'SWCorrect.dart';
import 'SW_Home_Page.dart';


class SWQuestionFormat3 extends StatelessWidget {
  const SWQuestionFormat3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints.expand(),
        decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(231, 117, 42, 237),
            Color.fromARGB(231, 232, 62, 93),
          ],
        ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0.6),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [SWBanner(), QuestionWidget3(), QuestionScrollable3()],
          ),
        ),
      ),
    );
  }
}

class QuestionWidget3 extends StatelessWidget {
  const QuestionWidget3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (225),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Center(
                  child: Text(
                    "What substance powers each lightsaber?",
                    textScaleFactor: 1.5,)
                  ))),
        ));
  }
}

class QuestionScrollable3 extends StatelessWidget {
  const QuestionScrollable3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          Question3Answer1(),
          Question3Answer2(),
          Question3Answer3(),
          Question3Answer4(),
          InQuestionBackButton(),
        ],
      ),
    );
  }
}

class Question3Answer1 extends StatelessWidget {
  const Question3Answer1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question3Answer1Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question3Answer1Design extends StatelessWidget {
  const Question3Answer1Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Kalashtar Crystals",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question3Answer2 extends StatelessWidget {
  const Question3Answer2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question3Answer2Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question3Answer2Design extends StatelessWidget {
  const Question3Answer2Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Imori Crystals",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question3Answer3 extends StatelessWidget {
  const Question3Answer3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question3Answer3Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWIncorrectBackground()),
        );
      },
    );
  }
}

class Question3Answer3Design extends StatelessWidget {
  const Question3Answer3Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Coricidin Crystals",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}

class Question3Answer4 extends StatelessWidget {
  const Question3Answer4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const Question3Answer4Design(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SWCorrectBackground()),
        );
      },
    );
  }
}

class Question3Answer4Design extends StatelessWidget {
  const Question3Answer4Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Center(
          child: Container(
              height: (75),
              width: (300),
              decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                  offset: Offset(1, 1)
                  ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color.fromARGB(255, 162, 7, 193)),
              child: Container(
                  padding: const EdgeInsets.all(26),
                  child: Text(
                    "Kyber Crystals",
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.5,
                  ))),
        ));
  }
}