import 'package:flutter/material.dart';
import 'start.dart';
  
void main() {
  runApp(Welcome());
}
  
class Welcome extends StatelessWidget {
    
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'GeeksForGeeks',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        
      ),
      home:  StartScreen(),
    );
  }
}
