import 'package:airforcetriviaapp/main.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'final.dart';
class StartScreen extends StatelessWidget {
  const StartScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body:Container(
        decoration: BoxDecoration(
	   gradient: const LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromRGBO(207, 231, 216, 1),
              Color.fromRGBO(154, 150, 205, 25)
            ]),
	  border: Border.all(
		color: Color.fromARGB(255, 125, 125, 140),
		width: 10,
	  )),
      
        
        
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'TRIVIA',
                style: TextStyle(fontSize: 48.0),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => MyApp(),
                    ),
                  );
                },
                
                
                 
                 child: AnimatedTextKit(
    animatedTexts: [
      ColorizeAnimatedText(
        'START',
        textStyle: colorizeTextStyle,
        colors: colorizeColors,
      ),
      ColorizeAnimatedText(
        'START',
        textStyle: colorizeTextStyle,
        colors: colorizeColors,
      ),
      ColorizeAnimatedText(
        'START',
        textStyle: colorizeTextStyle,
        colors: colorizeColors,
      ),
    ],
    isRepeatingAnimation: true,
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MyApp()),
        );
    },
  ),
),
             ] )
            
          ),
        ),//c
      );
  }
}
const colorizeColors = [
  Color.fromARGB(255, 255, 255, 255),
  Color.fromARGB(255, 74, 158, 227),
  Color.fromARGB(255, 232, 219, 96),
  Color.fromARGB(255, 240, 90, 79),
];

const colorizeTextStyle = TextStyle(
  fontSize: 50.0,
  fontFamily: 'Horizon',
);

