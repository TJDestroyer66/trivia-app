

import 'package:flutter/material.dart';

void main() {
  runApp(AnimationDemo());
}

class AnimationDemo extends StatefulWidget {
  @override
  createState() => _AnimationDemoState();
}

class _AnimationDemoState extends State<AnimationDemo>  with TickerProviderStateMixin{late AnimationController _controller;
@override
void initState() {
    
    super.initState();
    _controller = AnimationController(
      value: 50,
      lowerBound: 50,
      upperBound: 300,
      duration: Duration(seconds:1),
      vsync: this,
    );
  }
  @override
  Widget build(Context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: Scaffold(
            body:GestureDetector(child:
             Center(
      child:
       AnimatedBuilder(
        animation: _controller,
       builder: (context, child){
return Container(
  child: child,
  height: _controller.value,
  width:_controller.value,

);
       },

       child: Container(
        color:Colors.red,
        constraints: BoxConstraints.expand(),
       
       
       ),
       )
       
       )
        
        ),
    ));
  }
}
