class Question {
  String questionText;

  List<String> Incorrectanswers;
  String Correctanswer;

  Question(this.questionText, this.Incorrectanswers, this.Correctanswer);
}

List<Question> DnDDatabase = [
  Question(
    'How many Official Classes are there in DnD 5th Edition?',
    [
      '12',
      '14',
      '15',
    ],
    '13',
  ),
  Question(
    "What is the highest level a DnD Character can reach?",
    [
      '15',
      '25',
      '30',
    ],
    '20',
  ),
  Question(
    'Which die is rolled the most in DnD?',
    [
      '6 sided die',
      '8 sided die',
      '12 sided die',
    ],
    '20 sided die',
  ),
  Question(
    'How many Ability Scores are there in DnD?',
    [
      '4',
      '5',
      '8',
    ],
    '6',
  ),
  Question(
    'What is the Most Deadly Creature in the Monster Manual?',
    [
      'Ancient Red Dragon',
      'Ancient Gold Dragon',
      'Kraken',
    ],
    'Tarrasque',
  ),
  Question(
    "How many levels of spells are there in DnD?",
    [
      '9',
      '12',
      '15',
    ],
    '10',
  ),
  Question(
    "What is the most powerful spell in DnD?",
    [
      "Time Ravage",
      'Power Word Kill',
      'Invulnerability',
    ],
    'Wish',
  ),
  Question(
    'Which Ability Score is the Perception Skill associated with?',
    [
      'Intelligence',
      'Charisma',
      'Dexterity',
    ],
    'Wisdom',
  ),
  Question(
    'How many Editions of DnD have there been',
    [
      '4',
      '5',
      '7',
    ],
    '6',
  ),
  Question(
    'How many different types of dice are needed to play DnD?',
    [
      '5',
      '8',
      '11',
    ],
    '7',
  ),
];

List<Question> MarvelDCDatabase = [
  Question(
    'Which DC Superhero has the last name Allen?',
    [
      'Batman',
      'Superman',
      'Wonderwoman',
    ],
    'Flash',
  ),
  Question(
    "What is Batman's real first name?",
    [
      'Brandon',
      'Byran',
      'Bob',
    ],
    'Bruce',
  ),
  Question(
    'Who is the strongest Marvel Superhero?',
    [
      'Ironman',
      'Thor',
      'Captain America',
    ],
    'Hulk',
  ),
  Question(
    'Which Superhero is also known as the Man of Steel?',
    [
      'Flash',
      'Batman',
      'Green Lantern',
    ],
    'Superman',
  ),
  Question(
    'Which Marvel Superhero is the smartest?',
    [
      'Tony Stark',
      'Bruce Banner',
      'Hank Pym',
    ],
    'Reed Richarads',
  ),
  Question(
    "What is Hawkeye's real name?",
    [
      'Connor',
      'Cory',
      'Cole',
    ],
    'Clint',
  ),
  Question(
    "What is the name of Thor's axe?",
    [
      "The Headman's Axe",
      'The Power Axe',
      'Wuuthrad',
    ],
    'Storm-Breaker',
  ),
  Question(
    'How many Infinity Stones are there?',
    [
      '4',
      '3',
      '5',
    ],
    '6',
  ),
];

List<Question> StormlightDatabase = [
  Question(
    "What is the name of Kaladin's brother?",
    [
      'Teft',
      'Tom',
      'Tyson',
    ],
    'Tien',
  ),
  Question(
    "What is the name of Shallan's oldest brother?",
    [
      'Chuck',
      'Byran',
      'Tom',
    ],
    'Helaran',
  ),
  Question(
    'Which bridge crew does Kaladin join?',
    [
      '3',
      '2',
      '1',
    ],
    '4',
  ),
  Question(
    "What is Dalinar's nickname during the war?",
    [
      'Man of Steel',
      'Stormblessed',
      'Warbreaker',
    ],
    'Blackthorn',
  ),
  Question(
    "What is the name of Kaladin's Spren?",
    [
      'Pattern',
      'Ivory',
      'Stormfather',
    ],
    'Sly',
  ),
  Question(
    "Which weapon does Kaladin prefer to use?",
    [
      'Sword',
      'Staff',
      'His fists',
    ],
    'Spear',
  ),
  Question(
    "What is Kaladin's nickname on his bridge crew?",
    [
      "Blackthorn",
      'Stormfather',
      'Oathbringer',
    ],
    'Stormblessed',
  ),
  Question(
    'What type of spren does Kaladin first thing Sly is?',
    [
      'Glory',
      'Honor',
      'Pain',
    ],
    'Wind',
  ),
];

List<Question> CandyDatabase = [
  Question(
    'What country invented Chocolate?',
    [
      'Switzerland',
      'Norway',
      'Germany',
    ],
    'Mexico',
  ),
  Question(
    "What candy is banned in Europe",
    [
      'Kit Kat',
      'Snickers',
      'Twizzlers',
    ],
    'Skittles',
  ),
  Question(
    "When was the brand M&Ms invented?",
    [
      '1715',
      '1756',
      '1841',
    ],
    '1941',
  ),
  Question(
    'What is the most popular candy in the U.S.?',
    [
      'Sour Patch Kids',
      'Snickers',
      'Whoppers',
    ],
    "Resse's peanut butter cups",
  ),
  Question(
    'What is the most hated candy in the world?',
    [
      'Smarties',
      'Black Licorice',
      'Tootsie Rolls',
    ],
    'Candy Corn',
  ),
  Question(
    "How many pounds of candy are sold annually on Halloween?",
    [
      '100 million',
      '200 million',
      '800 million',
    ],
    '600 million',
  ),
];

List<Question> SportDatabase = [
  Question(
    'What is the most liked Sport in the world?',
    [
      'Basketball',
      'Baseball',
      'Hockey',
      'Football',
    ],
    'Soccer',
  ),
  Question(
    "What is the longest baseball game ever recorded?",
    [
      '6 hours 30 min',
      '7 hours 35 min',
      '7 hours',
    ],
    '8 Hours 6 min',
  ),
  Question(
    'What is the most popular sport in America?',
    [
      'Basketball',
      'Baseball',
      'Hockey',
      'Soccer',
    ],
    'Football',
  ),
  Question(
    'When was baseball invented?',
    [
      '1976',
      '1982',
      '1902',
      '1850',
    ],
    '1876',
  ),
  Question(
    'When was Football invented?',
    [
      '1910',
      '1905',
      '1855',
      '1890',
    ],
    '1920',
  ),
];

List<Question> AFDatabase = [
  Question(
    'How many types of planes does the air force use?',
    [
      '63',
      '47',
      '38',
    ],
    '39',
  ),
  Question(
    "How many people work on Hill Air Force Base?",
    [
      '20,000',
      '30,000',
      '18,000',
    ],
    '22,000',
  ),
  Question(
    'Which planes does Hill Air Force Base preform maintence on?',
    [
      'F-35',
      'F-16',
      'A-10',
    ],
    'All of the above',
  ),
  Question(
    'What year was Hill Air Force Base created?',
    [
      '1935',
      '1936',
      '1938',
    ],
    '1939',
  ),
  Question(
    'Which is higher than a group',
    [
      'squadron',
      'Section',
      'all of the above',
    ],
    'Wing',
  ),
  Question(
    "Which of these are higher ranked Air Force Officers?",
    [
      'captian',
      'colonel',
      'major',
    ],
    'lieutenant general',
  ),
  Question(
    "when was the Air Force founded",
    [
      "September 8, 1947",
      'September 18, 1946',
      'September 8 1947',
    ],
    'September 18, 1947',
  ),
];

List<Question> GeneralDatabase =
    DnDDatabase + MarvelDCDatabase + StormlightDatabase + CandyDatabase + SportDatabase + AFDatabase;
List<Question> TempGeneralDatabase = GeneralDatabase;
