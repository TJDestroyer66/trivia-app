import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import '../main.dart';
import 'General_globals.dart' as globals;
import 'General_Question_Database.dart';

class GeneralHomePage extends StatelessWidget {
  const GeneralHomePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Random random = Random();
    int questionNumberIndex = random.nextInt(globals.newIndex);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: GeneralQuestionDetail(
        question: TempGeneralDatabase[questionNumberIndex],
        questionNumberIndex: questionNumberIndex,
      ),
    );
  }
}

class GeneralQuestionDetail extends StatefulWidget {
  final Question question;
  final int questionNumberIndex;

  const GeneralQuestionDetail({
    Key? key,
    required this.question,
    required this.questionNumberIndex,
  }) : super(key: key);

  @override
  State<GeneralQuestionDetail> createState() => _GeneralQuestionDetailState();
}

class _GeneralQuestionDetailState extends State<GeneralQuestionDetail> {
  @override
  Widget build(BuildContext context) {
    int count = 0;
    Random random = Random();
    int correctIndex = random.nextInt(4);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          toolbarHeight: MediaQuery.of(context).size.height * .1,
          title: const Text(
            'General',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 60,
              color: Colors.white,
            ),
          ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(148, 118, 166, 100),
                  Color.fromRGBO(89, 41, 101, 100),
                ],
              ),
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(20),
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                border: Border.all(
                  color: const Color.fromRGBO(63, 63, 63, 62),
                  width: 3,
                ),
                gradient: const LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color.fromRGBO(148, 118, 166, 100),
                    Color.fromRGBO(89, 41, 101, 100),
                  ],
                ),
                borderRadius: const BorderRadius.all(Radius.circular(35)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(1),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Text(
                widget.question.questionText,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 40,
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                padding: const EdgeInsets.only(top: 10),
                itemCount: widget.question.Incorrectanswers.length + 1,
                itemBuilder: (BuildContext context, int index) {
                  if (index == correctIndex) {
                    return optionButton(
                      context,
                      widget.question.Correctanswer,
                      () {
                        if (globals.countQuestion == 19) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const GeneralFinalPage()),
                          );
                        } else {
                          globals.countQuestion++;
                          TempGeneralDatabase.removeAt(
                              widget.questionNumberIndex);
                          globals.newIndex--;
                          globals.GeneralScore++;
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const GeneralHomePage()),
                          );
                        }
                      },
                    );
                  } else {
                    count++;
                    return optionButton(
                      context,
                      widget.question.Incorrectanswers[count - 1],
                      () {
                        if (globals.countQuestion == 19) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const GeneralFinalPage()),
                          );
                        } else {
                          globals.countQuestion++;
                          TempGeneralDatabase.removeAt(
                              widget.questionNumberIndex);
                          globals.newIndex--;
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const GeneralHomePage()),
                          );
                        }
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget optionButton(
    BuildContext context, String option, Function pageNavigation) {
  return GestureDetector(
    child: Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.all(15),
      height: MediaQuery.of(context).size.height * .1,
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromRGBO(63, 63, 63, 62),
          width: 3,
        ),
        gradient: const LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(148, 118, 166, 100),
            Color.fromRGBO(89, 41, 101, 100),
          ],
        ),
        borderRadius: const BorderRadius.all(Radius.circular(35)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          option,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 32,
            color: Color.fromARGB(255, 255, 255, 255),
          ),
        ),
      ),
    ),
    onTap: () {
      pageNavigation();
    },
  );
}

class GeneralFinalPage extends StatelessWidget {
  const GeneralFinalPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int score = globals.GeneralScore;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          toolbarHeight: MediaQuery.of(context).size.height * .1,
          title: const FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              'General',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 60,
                color: Colors.white,
              ),
            ),
          ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(148, 118, 166, 100),
                  Color.fromRGBO(89, 41, 101, 100),
                ],
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const MyApp()),
              );
            },
            icon: const Icon(
              Icons.house,
              size: 50,
            ),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: Align(
                alignment: Alignment.center,
                child: AnimatedTextKit(
                  repeatForever: true,
                  animatedTexts: [
                    ColorizeAnimatedText(
                      'You got $score/20',
                      textAlign: TextAlign.center,
                      textStyle: GeneralcolorizeTextStyle,
                      colors: GeneralcolorizeColors,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


List<MaterialColor> GeneralcolorizeColors = [
  Colors.red,
  Colors.yellow,
  Colors.purple,
  Colors.blue,
];

const GeneralcolorizeTextStyle = TextStyle(
  fontSize: 250.0,
  fontFamily: 'SF',
  fontWeight: FontWeight.bold,
);