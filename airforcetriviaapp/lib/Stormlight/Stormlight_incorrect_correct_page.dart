import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'Stormlight_First_Page.dart';
import 'Stormlight_globals.dart' as globals;

class StormlightIncorrectTextDesign extends StatelessWidget {
  const StormlightIncorrectTextDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Flexible(
          child: Text(
            'Incorrect',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 40,
              color: Colors.white,
            ),
          ),
        ),
        Flexible(
          child: Text(
            'X',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.red,
              fontSize: 50,
            ),
          ),
        ),
      ],
    );
  }
}

class StormlightIncorrectButton extends StatelessWidget {
  const StormlightIncorrectButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(156, 65, 60, 61),
            Color.fromRGBO(255, 0, 0, 100),
          ],
        ),
      ),
      child: const StormlightIncorrectTextDesign(),
    );
  }
}

//backgroundColor: Color.fromARGB(255, 177, 44, 44),
class StormlightIncorrectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    globals.StormlightScore--;
    return Material(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: const Color.fromRGBO(9, 21, 60, 1),
          appBar: AppBar(
            centerTitle: true,
            toolbarHeight: MediaQuery.of(context).size.height * .1,
            title: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'Stormlight',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 60,
                  color: Color.fromARGB(255, 237, 237, 239),
                ),
              ),
            ),
            flexibleSpace: Container(
              color: Color.fromARGB(255, 142, 143, 147),
            ),
          ),
          body: GestureDetector(
            child: const StormlightIncorrectButton(),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const StormlightStartPage()),
              );
            },
          ),
        ),
      ),
    );
  }
}

class StormlightCorrectTextDesign extends StatelessWidget {
  const StormlightCorrectTextDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AnimatedTextKit(
          animatedTexts: [
            ColorizeAnimatedText(
              'Correct',
              textStyle: StormlightsColorizeTextStyle,
              colors: StormlightsColorizeColors,
            ),
          ],
        ),
        const Icon(
          Icons.check_sharp,
          color: Color.fromRGBO(95, 210, 95, 100),
          size: 250,
        ),
      ],
    );
  }
}

class StormlightCorrectButton extends StatelessWidget {
  const StormlightCorrectButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(156, 67, 72, 66),
            Color.fromRGBO(36, 255, 0, 100),
          ],
        ),
      ),
      child: const StormlightCorrectTextDesign(),
    );
  }
}

//backgroundColor: Color.fromARGB(255, 177, 44, 44),
class StormlightCorrectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: const Color.fromRGBO(9, 21, 60, 1),
          appBar: AppBar(
            centerTitle: true,
            toolbarHeight: MediaQuery.of(context).size.height * .1,
            title: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'Stormlight',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 60,
                  color: Color.fromARGB(255, 237, 237, 239),
                ),
              ),
            ),
            flexibleSpace: Container(
              color: Color.fromARGB(255, 142, 143, 147),
            ),
          ),
          body: GestureDetector(
            child: const StormlightCorrectButton(),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const StormlightStartPage()),
              );
            },
          ),
        ),
      ),
    );
  }
}

List<MaterialColor> StormlightsColorizeColors = [
  Colors.red,
  Colors.yellow,
  Colors.purple,
  Colors.blue,
];

const StormlightsColorizeTextStyle = TextStyle(
  fontSize: 40.0,
  fontFamily: 'SF',
);
