import 'package:airforcetriviaapp/main.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import '../Andrea/question A.dart';
import '../Jackson/DnD_Home_Page.dart';
import '../General/General_Question_Database.dart';
import 'Stormlight_Question_detail.dart';
import 'Stormlight_globals.dart' as globals;

//Stormlight's question button
class StormlightQuestionButtonTemplate extends StatelessWidget {
  StormlightQuestionButtonTemplate({Key? key, required this.buttonIndex})
      : super(key: key);

  int buttonIndex;

  @override
  Widget build(BuildContext context) {
    const snackBar = SnackBar(content: Text('Question already answered!'));
    return GestureDetector(
      child: StormlightQuestionButtonDesign(buttonIndex: buttonIndex),
      onTap: () {
        if (globals.StormlightAnsweredQuestions[buttonIndex] == false) {
          globals.StormlightAnsweredQuestions[buttonIndex] = true;
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return StormlightQuestionDetail(question: StormlightDatabase[buttonIndex]);
              },
            ),
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      },
    );
  }
}

//Stormlight's question button design
class StormlightQuestionButtonDesign extends StatelessWidget {
  StormlightQuestionButtonDesign({Key? key, required this.buttonIndex})
      : super(key: key);

  int buttonIndex;
// Questions
  @override
  Widget build(BuildContext context) {
    int questionIndex = buttonIndex + 1;
    return Material(
      type: MaterialType.transparency,
      child: Container(
        margin: const EdgeInsets.all(15),
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 148, 160, 186),
          border: Border.all(
            color: const Color.fromARGB(194, 0, 0, 0),
            width: 3,
          ),
          // gradient: const LinearGradient(
          //   begin: Alignment.topLeft,
          //   end: Alignment.bottomRight,
          //   colors: [
          //     Color.fromARGB(246, 255, 244, 244),
          //     Color.fromARGB(248, 255, 255, 255),
          //   ],
          // ),
          borderRadius: const BorderRadius.all(Radius.circular(35)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(1),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: AnimatedTextKit(
            animatedTexts: [
              WavyAnimatedText('Question $questionIndex',
                  textAlign: TextAlign.center,
                  textStyle: const TextStyle(
                    fontSize: 32,
                    color: Colors.white,
                  ),
                  speed: const Duration(milliseconds: 300)),
            ],
            totalRepeatCount: 1,
          ),
        ),
      ),
    );
  }
}

//This runs all the other classes before and formats them so they look nice.
class StormlightStartPage extends StatefulWidget {
  const StormlightStartPage({
    Key? key,
  }) : super(key: key);

  @override
  State<StormlightStartPage> createState() => _StormlightStartPage();
}

class _StormlightStartPage extends State<StormlightStartPage> {
  @override
  Widget build(BuildContext context) {
    int score = globals.StormlightScore;
    return Material(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: const Color.fromRGBO(9, 21, 60, 1),
          appBar: AppBar(
            centerTitle: true,
            toolbarHeight: MediaQuery.of(context).size.height * .1,
            title: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'Stormlight',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 60,
                  color: Color.fromARGB(255, 237, 237, 239),
                ),
              ),
            ),
            flexibleSpace: Container(
              color: Color.fromARGB(255, 142, 143, 147),
            ),
            leading: IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyApp()),
                );
              },
              icon: const Icon(
                Icons.house,
                size: 50,
              ),
            ),
          ),
          body: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: StormlightDatabase.length,
                  itemBuilder: (BuildContext context, int index) {
                    return StormlightQuestionButtonTemplate(buttonIndex: index);
                  },
                ),
              ),
              Row(
                children: [
                  const Expanded(child: StormlightPreviousButton()),
                  Expanded(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        'Score: $score/8',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  const Expanded(child: StormlightNextButton()),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//Stormlight's Next button
class StormlightNextButton extends StatelessWidget {
  const StormlightNextButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const StormlightNextButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AWidget()),
        );
      },
    );
  }
}

//Stormlight's Next button design
class StormlightNextButtonDesign extends StatelessWidget {
  const StormlightNextButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 142, 143, 147),
        border: Border.all(
          color: const Color.fromRGBO(63, 63, 63, 62),
          width: 3,
        ),
        borderRadius: const BorderRadius.all(Radius.circular(35)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: const Icon(
        Icons.keyboard_arrow_right_outlined,
        size: 55,
        color: Colors.white,
      ),
    );
  }
}

//Stormlight's previous button
class StormlightPreviousButton extends StatelessWidget {
  const StormlightPreviousButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const StormlightPreviousButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const DnDWidget()),
        );
      },
    );
  }
}

//Stormlight's previous button design
class StormlightPreviousButtonDesign extends StatelessWidget {
  const StormlightPreviousButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 142, 143, 147),
        border: Border.all(
          color: const Color.fromRGBO(63, 63, 63, 62),
          width: 3,
        ),
        borderRadius: const BorderRadius.all(Radius.circular(35)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: const Icon(
        Icons.keyboard_arrow_left_outlined,
        size: 55,
        color: Colors.white,
      ),
    );
  }
}
