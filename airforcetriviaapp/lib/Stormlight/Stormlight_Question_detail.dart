import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import '../General/General_Question_Database.dart';
import 'dart:math';
import 'Stormlight_globals.dart' as globals;
import 'Stormlight_incorrect_correct_page.dart';

class StormlightQuestionDetail extends StatefulWidget {
  final Question question;

  const StormlightQuestionDetail({
    Key? key,
    required this.question,
  }) : super(key: key);

  @override
  State<StormlightQuestionDetail> createState() => _StormlightQuestionDetail();
}

class _StormlightQuestionDetail extends State<StormlightQuestionDetail> {
  @override
  Widget build(BuildContext context) {
    int count = 0;
    Random random = Random();
    int correctIndex = random.nextInt(4);
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color.fromRGBO(9, 21, 60, 1),
        appBar: AppBar(
          centerTitle: true,
          toolbarHeight: MediaQuery.of(context).size.height * .1,
          title: const FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              'Stormlight',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 60,
                color: Color.fromARGB(255, 237, 237, 239),
              ),
            ),
          ),
          flexibleSpace: Container(
            color: Color.fromARGB(255, 142, 143, 147),
          ),
        ),
        body: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(15),
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 148, 160, 186),
                border: Border.all(
                  color: const Color.fromARGB(194, 0, 0, 0),
                  width: 3,
                ),
                borderRadius: const BorderRadius.all(Radius.circular(35)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(1),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: AnimatedTextKit(
                  totalRepeatCount: 1,
                  animatedTexts: [
                    TypewriterAnimatedText(widget.question.questionText,
                        textAlign: TextAlign.center,
                        textStyle: const TextStyle(
                          fontSize: 40,
                          color:  Color.fromARGB(194, 0, 0, 0),
                        ),
                        curve: Curves.easeIn,
                        speed: const Duration(milliseconds: 80)),
                  ],
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                padding: const EdgeInsets.only(top: 10),
                itemCount: widget.question.Incorrectanswers.length + 1,
                itemBuilder: (BuildContext context, int index) {
                  if (index == correctIndex) {
                    globals.StormlightScore++;
                    return optionButton(
                      context,
                      widget.question.Correctanswer,
                      () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => StormlightCorrectPage()),
                        );
                      },
                    );
                  } else {
                    count++;
                    return optionButton(
                      context,
                      widget.question.Incorrectanswers[count - 1],
                      () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => StormlightIncorrectPage()),
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget optionButton(
    BuildContext context, String option, Function pageNavigation) {
  return GestureDetector(
    child: Container(
      margin: const EdgeInsets.all(15),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 148, 160, 186),
        border: Border.all(
          color: const Color.fromARGB(194, 0, 0, 0),
          width: 3,
        ),
        borderRadius: const BorderRadius.all(Radius.circular(35)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: Align(
          alignment: Alignment.center,
          child: Text(
            option,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 32,
              color: Color.fromARGB(194, 0, 0, 0),
            ),
          ),
        ),
      ),
    ),
    onTap: () {
      pageNavigation();
    },
  );
}
