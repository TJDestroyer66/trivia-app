import 'package:airforcetriviaapp/DONT_DELETE_AGAIN.dart/WELCOME.dart';
import 'package:airforcetriviaapp/General/General_Question_Detail.dart';
import 'package:airforcetriviaapp/Stormlight/Stormlight_First_Page.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'Tyson/Tysons_First_Page.dart';
import 'Andrea/question A.dart';
import 'Connor/Connors_first_page.dart';
import 'Airforce/Airforce_first_page.dart';
import 'Jackson/DnD_Home_Page.dart';
import 'StarWars/SW_Home_Page.dart';
import 'package:flutter/material.dart';
import 'package:restart_app/restart_app.dart';

//Runs the flutter app
void main() {
  runApp(Welcome());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

// This is a class that creates a custom title bar for the home page
class MyCustomBanner extends StatelessWidget {
  const MyCustomBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromARGB(231, 176, 141, 231),
            Color.fromARGB(231, 149, 156, 238),
            Color.fromARGB(255, 173, 115, 115),
            Color.fromARGB(255, 193, 175, 56),
            Color.fromARGB(255, 116, 114, 132),
          ],
        ),
      ),
      child: DefaultTextStyle(
        style: const TextStyle(
          fontSize: 80,
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: AnimatedTextKit(
          animatedTexts: [
            WavyAnimatedText(
              'TRIVIA',
              textAlign: TextAlign.center,
            ),
            WavyAnimatedText(
              'TRIVIA',
              textAlign: TextAlign.center,
            ),
          ],
          isRepeatingAnimation: true,
          onTap: () {
            //print("Tap Event");
          },
        ),
      ),
    );
  }
}

//Tyson's button
class SuperheroButton extends StatelessWidget {
  const SuperheroButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const SuperheroButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const TysonStartPage()),
        );
      },
    );
  }
}

// Tyson's SuperHero button design
class SuperheroButtonDesign extends StatelessWidget {
  const SuperheroButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(40),
        child: Center(
          child: Container(
              height: (75),
              width: (400),
              decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color.fromARGB(255, 203, 79, 79),
                        Color.fromARGB(255, 209, 41, 41),
                      ]),
                  borderRadius: BorderRadius.all(Radius.circular(75)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(25),
                  child: const Text(
                    "Marvel/DC",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}

// Andrea's candy button class
class CandyButton extends StatelessWidget {
  const CandyButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const CandyButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AWidget()),
        );
      },
    );
  }
}

// Andrea's candy button design class
class CandyButtonDesign extends StatelessWidget {
  const CandyButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(40),
        child: Center(
          child: Container(
            height: (75),
            width: (400),
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black,
                      blurRadius: 5,
                      spreadRadius: 1,
                      offset: Offset(1, 1)),
                ],
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromRGBO(237, 171, 171, 0.929),
                      Color.fromRGBO(202, 202, 205, 0.792),
                    ]),
                borderRadius: BorderRadius.all(Radius.circular(75)),
                color: Color.fromARGB(255, 152, 152, 152)),
            child: Row(children: [
              const Expanded(
                  flex: 1,
                  child:
                      Image(image: AssetImage('assets/Sprinkles_Right.png'))),
              Expanded(
                  flex: 1,
                  child: Container(
                      padding: const EdgeInsets.all(25),
                      child: const Text(
                        "Candy",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.4,
                      ))),
              const Expanded(
                  flex: 1,
                  child: Image(image: AssetImage('assets/Sprinkles_Left.png'))),
            ]),
          ),
        ));
  }
}

// Sports button class
class SportsButton extends StatelessWidget {
  const SportsButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const SportsButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const CWidget()),
        );
      },
    );
  }
}

// Sports button image class
class SportsButtonDesign extends StatelessWidget {
  const SportsButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(40),
        child: Center(
          child: Container(
            height: (75),
            width: (400),
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black,
                      blurRadius: 5,
                      spreadRadius: 1,
                      offset: Offset(1, 1)),
                ],
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromARGB(255, 255, 243, 7),
                      Color.fromARGB(255, 158, 103, 31),
                    ]),
                borderRadius: BorderRadius.all(Radius.circular(75)),
                color: Color.fromARGB(255, 152, 152, 152)),
            child: Row(children: [
              const Expanded(
                  flex: 1,
                  child: Image(
                      image: AssetImage('assets/SPORTS/BaseBall_Left.png'))),
              Expanded(
                  flex: 1,
                  child: Container(
                      padding: const EdgeInsets.all(25),
                      child: const Text(
                        "Sports",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.4,
                      ))),
              const Expanded(
                  flex: 1,
                  child: Image(
                      image: AssetImage('assets/SPORTS/BaseBall_Right.png'))),
            ]),
          ),
        ));
  }
}

// Airforce button class
class AirforceButton extends StatelessWidget {
  const AirforceButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const AirForceButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AFWidget()),
        );
      },
    );
  }
}

// Airforce button image class
class AirForceButtonDesign extends StatelessWidget {
  const AirForceButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(40),
        child: Center(
          child: Container(
            height: (75),
            width: (400),
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black,
                      blurRadius: 5,
                      spreadRadius: 1,
                      offset: Offset(1, 1)),
                ],
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromARGB(255, 22, 18, 138),
                      Color.fromARGB(255, 76, 129, 169),
                    ]),
                borderRadius: BorderRadius.all(Radius.circular(75)),
                color: Color.fromARGB(255, 152, 152, 152)),
            child: Row(children: [
              const Expanded(
                  flex: 1,
                  child: Image(image: AssetImage('assets/Airplane_Left.png'))),
              Expanded(
                  flex: 1,
                  child: Container(
                      padding: const EdgeInsets.all(25),
                      child: const Text(
                        "Airforce",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.5,
                      ))),
              const Expanded(
                  flex: 1,
                  child: Image(image: AssetImage('assets/Airplane_Right.png'))),
            ]),
          ),
        ));
  }
}

// DND button class
class DnDButton extends StatelessWidget {
  const DnDButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const DnDButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const DnDWidget()),
        );
      },
    );
  }
}

// DND button image class
class DnDButtonDesign extends StatelessWidget {
  const DnDButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 80, bottom: 40),
        // padding: const EdgeInsets.all(40),
        child: Center(
          child: Container(
            height: (75),
            width: (400),
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black,
                      blurRadius: 5,
                      spreadRadius: 1,
                      offset: Offset(1, 1)),
                ],
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromARGB(255, 167, 0, 179),
                      Color.fromARGB(255, 0, 0, 0),
                    ]),
                borderRadius: BorderRadius.all(Radius.circular(75)),
                color: Color.fromARGB(255, 152, 152, 152)),
            child: Row(children: [
              const Expanded(
                  flex: 1,
                  child: Image(
                      image: AssetImage('assets/D&D/Dice_Icon_Updated.png'))),
              Expanded(
                  flex: 5,
                  child: Container(
                      padding: const EdgeInsets.all(25),
                      child: const Text(
                        "D & D",
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.4,
                      ))),
              const Expanded(
                  flex: 1,
                  child: Image(
                      image: AssetImage('assets/D&D/Dice_Icon_Updated.png'))),
            ]),
          ),
        ));
  }
}

// StarWars button class
class StarWarsButton extends StatelessWidget {
  const StarWarsButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const StarWarsButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const SWWidget()),
        );
      },
    );
  }
}

// DND button image class
class StarWarsButtonDesign extends StatelessWidget {
  const StarWarsButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(40),
        child: Center(
          child: Container(
              height: (75),
              width: (400),
              decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color.fromARGB(255, 252, 39, 46),
                        Color.fromARGB(255, 75, 114, 255),
                      ]),
                  borderRadius: BorderRadius.all(Radius.circular(75)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(25),
                  child: const Text(
                    "Star Wars",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}

// General button class
class GeneralButton extends StatelessWidget {
  const GeneralButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const GeneralButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const GeneralHomePage()),
        );
      },
    );
  }
}

// General button image class
class GeneralButtonDesign extends StatelessWidget {
  const GeneralButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(40),
        child: Center(
          child: Container(
              height: (75),
              width: (400),
              decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color.fromARGB(255, 189, 60, 232),
                        Color.fromARGB(255, 95, 46, 46),
                      ]),
                  borderRadius: BorderRadius.all(Radius.circular(75)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(25),
                  child: const Text(
                    "General",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}

class RestartButton extends StatelessWidget {
  const RestartButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const RestartButtonDesign(),
        onTap: () => {Restart.restartApp(webOrigin: 'MyApp')});
  }
}

// General button image class
class RestartButtonDesign extends StatelessWidget {
  const RestartButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(40),
        child: Center(
          child: Container(
              height: (75),
              width: (400),
              decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black,
                        blurRadius: 5,
                        spreadRadius: 1,
                        offset: Offset(1, 1)),
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color.fromARGB(255, 255, 255, 255),
                        Color.fromARGB(255, 138, 138, 138),
                      ]),
                  borderRadius: BorderRadius.all(Radius.circular(75)),
                  color: Color.fromARGB(255, 152, 152, 152)),
              child: Container(
                  padding: const EdgeInsets.all(25),
                  child: const Text(
                    "Reset App",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.4,
                  ))),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * .090,
      decoration: const BoxDecoration(color: Colors.grey),
      child: Scaffold(
        backgroundColor: Colors.white.withOpacity(0),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [MyCustomBanner(), HomePageButtons()],
        ),
      ),
    ));
  }
}

// Stormlight button class
class StormlightButton extends StatelessWidget {
  const StormlightButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: const StormlightButtonDesign(),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const StormlightStartPage()),
        );
      },
    );
  }
}

// Stormlight button design class
class StormlightButtonDesign extends StatelessWidget {
  const StormlightButtonDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(40),
        child: Center(
          child: Container(
            height: (75),
            width: (400),
            decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Colors.black,
                    blurRadius: 5,
                    spreadRadius: 1,
                    offset: Offset(1, 1)),
              ],
              borderRadius: BorderRadius.all(Radius.circular(75)),
              color: Color.fromARGB(255, 7, 16, 45),
            ),
            child: Row(children: [
              const Expanded(
                  flex: 1,
                  child:
                      Image(image: AssetImage('assets/Stormlight/Sword.png'))),
              Expanded(
                  flex: 1,
                  child: Container(
                      padding: const EdgeInsets.all(25),
                      child: const Text(
                        "Stormlight",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.25,
                      ))),
              const Expanded(
                  flex: 1,
                  child:
                      Image(image: AssetImage('assets/Stormlight/Sword.png'))),
            ]),
          ),
        ));
  }
}

//Calls button classes to display buttons on home page
class HomePageButtons extends StatelessWidget {
  const HomePageButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: const <Widget>[
          DnDButton(),
          SuperheroButton(),
          CandyButton(),
          SportsButton(),
          StormlightButton(),
          AirforceButton(),
          StarWarsButton(),
          GeneralButton(),
          RestartButton(),
        ],
      ),
    );
  }
}
